var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_product_variant';

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
    product: req.body.product,
      name: req.body.name
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    product: req.body.product,
      name: req.body.name
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

module.exports = router;