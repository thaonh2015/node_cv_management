var jwt = require('jsonwebtoken');

var verifyToken = function(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if(typeof bearerHeader !== 'undefined') {
      // Split at the space
      const bearer = bearerHeader.split(' ');
      // Get token from array
      const bearerToken = bearer[1];
      // Set the token
      //req.token = bearerToken;
      // Next middleware
      if (bearerToken === 'no-check'){
          next();
      }else{
        jwt.verify(bearerToken, 'secretkey', (err, authData) => {
            if(err) {
              console.log(err);
              res.send(403, 'Forbidden! ' +  err.message);
            } else {
              next();
            }
          });
      }
      
    } else {
      // Forbidden
      res.send(403, 'Forbidden! You don\'t have the token to access.');
    }
  
}

module.exports =  verifyToken;