var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_session';

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
      store: req.body.store,
      device: req.body.device,
      schedule: req.body.schedule,
      employee: req.body.employee
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    store: req.body.store,
      device: req.body.device,
      schedule: req.body.schedule,
      employee: req.body.employee
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });


// other stuff

router.post('/open', async function(req, res) {
  if (req.body.employee_id && req.body.store_id && req.body.device_id) {

    var exist = await knex('pos_session').select('uuid').whereNull('end').where({
      employee: req.body.employee_id,
      store: req.body.store_id ,
      device: req.body.device_id 
    }).then(data => {
      return data[0];
    })

    if (exist){
      return res.json({
        status: false,
        message: "Sorry, you cannot open session. Re-open valid session." + exist.uuid
      });
    }

    var cash = req.body.cash_begin? req.body.cash_begin: 0;
    knex('pos_schedule').select('uuid').where({'store': req.body.store_id})
    .then(rs => {
      return knex('pos_session').insert({
        employee: req.body.employee_id,
        store: req.body.store_id,
        device: req.body.device_id,
        schedule: rs[0].uuid,
        cash_begin: cash
      }).returning('*')
    }).then(data => {
      //console.log('result open session {}', data)
     
      knex('pos_session')
      .select('pos_session.*', 'pos_store.name as store_name', 
      'pos_device.name as device_name', 'pos_schedule.name as schedule_name')
      .join('pos_store', {'pos_session.store': 'pos_store.uuid'})
      .join('pos_device', {'pos_session.device': 'pos_device.uuid'})
      .join('pos_schedule', {'pos_session.schedule': 'pos_schedule.uuid'})
      .where({'pos_session.uuid': data[0] })
      .then(ses => {
        return res.json({
          status: true,
          result: ses[0]
        });
      })
      
    })
    .catch(err => {
      console.log('err open session {}', err)
      return res.json({
        status: false,
        message: "Sorry, you cannot open session. Try again."
      });
    })
  } else {
    return res.json({
      status: false,
      message: "Employee, Store and Device are required."
    });
  }    
});

router.post('/close', async function(req, res) {
  if (req.body.session_id) {
    //console.log('current time {}',knex.fn.now())
    var closed = await knex('pos_session').select('end').where({uuid: req.body.session_id })
    .then(data => {
      return data[0].end;
    })

    if (closed) {
      return res.json({
        status: false,
        message: "Session is closed already at " + closed
      });
    }
    knex('pos_session').update({
      end: knex.fn.now()
    }).where({uuid: req.body.session_id})
    .then(data => {
      console.log('result close data {}', data)
      return res.json({
        status: true,
        result: {
          closed_session_id: req.body.session_id
        }
        
      });
    })
    .catch(err => {
      console.log('err open session {}', err)
      return res.json({
        status: false,
        message: "Sorry, you cannot close this session. Try again."
      });
    })
    
  } else {
    return res.json({
      status: false,
      message: "Session id is required."
    });
  }    
});

router.post('/current', async function(req, res) {
  if (req.body.employee_id) {
    knex('pos_session').select('pos_session.*', 'pos_store.name as store_name', 
    'pos_device.name as device_name', 'pos_schedule.name as schedule_name')
    .join('pos_store', {'pos_session.store': 'pos_store.uuid'})
    .join('pos_device', {'pos_session.device': 'pos_device.uuid'})
    .join('pos_schedule', {'pos_session.schedule': 'pos_schedule.uuid'})
    .whereNull('pos_session.end').andWhere({
      'pos_session.employee': req.body.employee_id
    }).then(rs => {
      return res.json({
        status: true,
        result: rs
      });
    }).catch(err => {
      console.log('err fetch current {}',err)
      return res.json({
        status: false,
        message: "Fetch current session fail."
      });
    })
    
  } else {
    return res.json({
      status: false,
      message: "Employee id is required."
    });
  }    
});

router.post('/reopen', async function(req, res) {
  if (req.body.session_id) {
    var sess = await knex('pos_session')
    .select('pos_session.*', 'pos_store.name as store_name', 
    'pos_device.name as device_name', 'pos_schedule.name as schedule_name')
    .join('pos_store', {'pos_session.store': 'pos_store.uuid'})
    .join('pos_device', {'pos_session.device': 'pos_device.uuid'})
    .join('pos_schedule', {'pos_session.schedule': 'pos_schedule.uuid'})
    .where({'pos_session.uuid': req.body.session_id })
    .then(data => {
      console.log('repoen fetchone {}', data)
      return data;
    })

    if (sess.length == 0) {
      return res.json({
        status: false,
        message: "Session is not existed"
      });
    }

    if (sess[0].end != undefined) {
      return res.json({
        status: false,
        message: "Session is closed already at " + sess[0].end
      });
    }

    return res.json({
      status: true,
      result: sess[0]
    });
    
  } else {
    return res.json({
      status: false,
      message: "Session id is required."
    });
  }    
});

module.exports = router;