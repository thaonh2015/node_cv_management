var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_promotion',
    _ = require('lodash');

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
      store: req.body.store,
      code: req.body.code,
      name: req.body.name
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    store: req.body.store,
    code: req.body.code,
    name: req.body.name
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

// other stuff 

router.post('/get-by-store', async function(req, res) {
  if (req.body.store_id) {
    var tax = await knex('pos_tax').select().where({store: req.body.store_id})
    .then(function(data){
        return data[0];
    });

    var tax_bands = await knex('pos_tax_band').select()
    .where({'store': req.body.store_id})
    .then(function(data){
        return data;
    });

    var tax_band_detail = await knex('pos_tax_band_detail').select('pos_tax_band_detail.*')
    .join('pos_tax_band', {'pos_tax_band_detail.tax_base': 'pos_tax_band.uuid'})
    .where({'pos_tax_band.store': req.body.store_id})
    .then(function(data){
        return data;
    });
   
    tax_bands.forEach(band => {
      band.details = _.filter(tax_band_detail, {tax_base: band.uuid});
      band.details = band.details.map(item => _.omit(item, ['tax_base','uuid']));
      
    });

    tax.tax_bands = tax_bands;

    return res.json({
      status: true,
      result: tax
    });
    
  } else {
    return res.json({
      status: false,
      message: "Store id is required."
    });
  }  
});

router.post('/get-configuration', async function(req, res) {
  if (req.body.store_id) {
    var tax = await knex('pos_tax').select().where({store: req.body.store_id})
    .then(function(data){
        return data[0];
    });

    return res.json({
      status: true,
      result: tax
    });
    
  } else {
    return res.json({
      status: false,
      message: "Store id is required."
    });
  }  
});

module.exports = router;