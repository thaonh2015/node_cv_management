var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    _ = require('lodash'),
    authen = require('./_verify_token');

router.get('/', authen, function(req, res) {
    //console.log('token', req.token)
    knex('pos_store').select().orderBy('uuid').then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
    knex('pos_store').insert({
        code: req.body.code, 
        name: req.body.name
    }).returning('*')
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
    knex('pos_store').select().where({uuid: req.params.id}).then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
    knex('pos_store').update({
        code: req.body.code, 
        name: req.body.name})
    .where({uuid: req.params.id})
    .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
    knex('pos_store').delete().where({uuid: req.params.id}).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

//other stuff
router.get('/:id/fetch-all', async function(req, res) {
  var store = await knex('pos_store').select().where({uuid: req.params.id})
  .then(function(data){
      return data[0];
  });

  var categories = await knex('pos_category').select().where({store: req.params.id})
  .then(function(data){
      return data.map(item => {
        return {
          uuid: item.uuid,
          code: item.code,
          short_name: item.short_name,
          name: item.name,
          group: item.group,
        }
      });
  });

  var seats = await knex('pos_seat').select().where({store: req.params.id})
  .then(function(data){
      return data.map(item => {
        return {
          uuid: item.uuid,
          code: item.code,
          location: item.location
        }
      });
  });

  var products = await knex('pos_product')
  .select('uuid', 'code', 'short_name', 'name', 'group', 'price', 'category as cate_id', 'tax_class')
  .where({store: req.params.id})
  .then(function(data){
      // return data.map(item => {
      //   return {
      //     uuid: item.uuid,
      //     code: item.code,
      //     short_name: item.short_name,
      //     name: item.name,
      //     group: item.group,
      //     price: item.price,
      //     cate_id: item.category
      //   }
      // });
      return data;
  });

  // var tax_bands = await knex('pos_tax_band')
  // .select('uuid', 'name')
  // .where({store: req.params.id})
  // .then(function(data){
  //     //console.log('tax bands {}', data);
  //     return data;
  // });

  var tax_band_details = await knex('pos_tax_band_detail')
  .select('pos_tax_band_detail.*')
  .join('pos_tax_band', {'pos_tax_band_detail.tax_base': 'pos_tax_band.uuid'})
  .where({'pos_tax_band.store': req.params.id})
  .orderBy('pos_tax_band_detail.priority')
  .then(function(data){
      //console.log('tax bands details {}', data);
      return data;
  });

  var grpTaxbands = _.mapValues(
    _.groupBy(tax_band_details, 'tax_base'),
    clist => clist.map(car => {
      _.omit(car, 'tax_base');
      return car;
  }));

  //console.log('grpTaxbands {}', grpTaxbands);

  var variants = await knex('pos_product_variant').select('pos_product_variant.*', 'pos_product.uuid as prod_id')
  .join('pos_product', {'pos_product_variant.product': 'pos_product.uuid'})
  .where({'pos_product.store': req.params.id})
  .then(function(data){
      return data.map(item => {
        return {
          uuid: item.uuid,
          name: item.name,
          price: item.price,
          prod_id: item.prod_id
        }
      });
  });

  var specifications = await knex('pos_product_specification').select('pos_product_specification.*', 'pos_product.uuid as prod_id')
  .join('pos_product', {'pos_product_specification.product': 'pos_product.uuid'})
  .where({'pos_product.store': req.params.id})
  .then(function(data){
      return data.map(item => {
        return {
          uuid: item.uuid,
          name: item.name,
          group: item.group,
          prod_id: item.prod_id
        }
      });
  });

  if (req.query.group && req.query.group == 1) {
    var groupedCate = _.mapValues(
      _.groupBy(categories, 'group'),
      clist => clist.map(car => {
        _.omit(car, 'group');
         var subProduct = _.filter(products, {cate_id: car.uuid});
         var groupedSubProducts = _.mapValues(_.groupBy(subProduct, 'group'),
          plist => plist.map(p => {
            _.omit(p, 'group')
            var subVariants = _.filter(variants, {prod_id: p.uuid});
            p.variants = subVariants;
            var subSpecifications = _.filter(specifications, {prod_id: p.uuid});
            var groupedSpec = _.mapValues(_.groupBy(subSpecifications, 'group'),
              splist => splist.map(sp => _.omit(sp, 'group')));
            p.specifications = groupedSpec;
            if (p.tax_class) {
              var percent = _.find(grpTaxbands[p.tax_class], 
                function(o) { 
                  return o.city === '*' || o.city === store.city; });
              if (percent) {
                p.tax_percent = percent.tax_rate;
              } else {
                p.tax_percent = 0
              }
            } else {
              p.tax_percent = 0
            }
            return p;
          }));
         car.products = groupedSubProducts;
        return car;
      }));

    store.categories = groupedCate;
  }else{
    store.cateogries = categories;
    var productTaxed = products.map(item => {
      if (item.tax_class) {
        var percent = _.find(grpTaxbands[item.tax_class], 
          function(o) { 
            return o.city === '*' || o.city === store.city; });
        if (percent) {
          item.tax_percent = percent.tax_rate;
        } else {
          item.tax_percent = 0
        }
      } else {
        item.tax_percent = 0
      }
      return item;
    });
    store.products = productTaxed;
    store.variants = variants;
    store.specifications = specifications;
  }

  store.seats = seats;

  return res.json({
    status: true,
    result: store
  });

});

module.exports = router;