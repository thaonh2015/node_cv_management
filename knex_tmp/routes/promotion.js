var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_promotion',
    _ = require('lodash');

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
      store: req.body.store,
      code: req.body.code,
      name: req.body.name
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    store: req.body.store,
    code: req.body.code,
    name: req.body.name
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

// other stuff 

router.post('/get-by-store', async function(req, res) {
  if (req.body.store_id) {
    var promotions = await knex('pos_promotion').select().where({store: req.body.store_id})
    .then(function(data){
        return data;
    });

    var conditions = await knex('pos_condition').select('pos_condition.*')
    .join('pos_promotion', {'pos_condition.promotion': 'pos_promotion.uuid'})
    .where({'pos_promotion.store': req.body.store_id})
    .then(function(data){
        return data;
    });

    var effects = await knex('pos_effect').select('pos_effect.*')
    .join('pos_promotion', {'pos_effect.promotion': 'pos_promotion.uuid'})
    .where({'pos_promotion.store': req.body.store_id})
    .then(function(data){
        return data;
    });
   
    promotions.forEach(prom => {
      prom.conditions = _.filter(conditions, {promotion: prom.uuid});
      prom.conditions = prom.conditions.map(item => _.omit(item, ['promotion','uuid']));
      prom.effects = _.filter(effects, {promotion: prom.uuid});
      prom.effects = prom.effects.map(item => _.omit(item, ['promotion','uuid']));
    });

    return res.json({
      status: true,
      result: promotions
    });
    
  } else {
    return res.json({
      status: false,
      message: "Store id is required."
    });
  }  
});

module.exports = router;