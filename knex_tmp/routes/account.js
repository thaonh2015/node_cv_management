var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    bcrypt = require('bcrypt-nodejs'),
    tbl = 'pos_account';

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

// router.post('/', function(req, res) {
//   CRUD.create(tbl, {
//       name: req.body.name,
//       description: req.body.description
//     })
//     .then(function(data){
//         res.json(data);
//     }).catch(function(err){
//       console.error(err);
//       res.status(500).send(err.sqlMessage);
//     });
//   });

router.post('/', function(req, res) {
  bcrypt.hash(req.body.pass, 10, function(err, hash){
    if (err) {
      console.error(err);
      res.status(500).send(err.sqlMessage);
    } else {
      CRUD.create(tbl, {
        email: req.body.email,
        pass: hash,
        display_name: req.body.display_name,
        employee: req.body.employee
      })
      .then(function(data){
          res.json(data);
      }).catch(function(err1){
        console.error(err1);
        res.status(500).send(err1.sqlMessage);
      });
    }
  });
});

router.get('/:id', function(req, res) {
  //console.log('req {}', req)
  CRUD.get(tbl, req.params.id)
    .then(function(data){
      res.json({
        result: data[0],
        links: [
          {rel: '_self', method: 'GET', href: req.originalUrl},
          {rel: 'full', method: 'GET', href: req.originalUrl + '/full'},
          {rel: 'employee', method: 'GET', href: req.baseUrl.replace('account','employee/') + data[0].employee}
        ]
      });
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });
  
  router.get('/:id/full', async function(req, res) {
    //console.log('req {}', req)
    var acc = await CRUD.get(tbl, req.params.id)
    .then(function(data){
      return data[0];
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
    if (acc) {
      CRUD.get('pos_employee', acc.employee)
      .then(function (emps){
        acc.employee_ = emps[0];
        res.json({
          result: acc,
          links: [
            {rel: '_self', method: 'GET', href: req.originalUrl}
          ]
        });
      }).catch(function(err){
        console.error(err);
        res.status(500).send(err.sqlMessage);
      });
    }
    
  });

// router.put('/:id', function(req, res) {
//   CRUD.update(tbl, req.params.id, {
//     name: req.body.name,
//     description: req.body.description
//   }).then(function(data){
//         res.sendStatus(200);
//     }).catch(function(err){
//       console.error(err);
//       res.status(500).send(err.sqlMessage);
//     });
//   });

router.put('/:id', function(req, res) {
  if (req.body.pass) {
    bcrypt.hash(req.body.pass, 10, function(err, hash){
      if (err) {
        console.error(err);
        res.status(500).send(err.sqlMessage);
      } else {
        CRUD.update(tbl, req.params.id, {
          email: req.body.email,
          pass: hash,
          display_name: req.body.display_name,
          employee: req.body.employee
        })
        .then(function(data){
            res.json(data);
        }).catch(function(err1){
          console.error(err1);
          res.status(500).send(err1.sqlMessage);
        });
      }
    });
  } else {
    CRUD.update(tbl, req.params.id, {
      display_name: req.body.display_name,
      email: req.body.email
    }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  }
});

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/signin', function(req, res) {
  if (req.body.email == 'admin@posdemo.com' && req.body.pass == 'admin') {
    return res.json({
      status: true,
      user: {
        uuid: '1',
        display_name: 'Admin PosDemo'
      },
      token: '000-0000-000'
    });
  } else {
    return res.json({
      status: false,
      message: "Sorry, forbiden to access."
    });
  }    
});

//other stuff

router.post('/login', function(req, res) {
  if (req.body.email && req.body.pass) {

    knex('pos_account').select('*').where({'email': req.body.email})
    .then(rs => {
      console.log('check email {}', rs);
      if (rs && rs.length > 0){

        bcrypt.compare(req.body.pass, rs[0].pass, function(err, matched) {
          if (matched) {
            Promise.all([
              knex('pos_employee').select('store').where({'uuid': rs[0].employee}),
              knex('pos_device').select('pos_device.uuid', 'pos_device.name')
                .join('pos_store', {'pos_device.store': 'pos_store.uuid'})
                .join('pos_employee', {'pos_employee.store': 'pos_store.uuid'})
                .where({'pos_employee.uuid': rs[0].employee})
            ]).then(data => {
              console.log('data {}', data);
              return res.json({
                status: true,
                user: {
                  account_id: rs[0].uuid,
                  employee_id: rs[0].employee,
                  display_name: rs[0].display_name,
                  store_id: data[0][0].store,
                  devices: data[1].map(item => {
                    return {
                      id: item.uuid,
                      name: item.name
                    };
                  })
                },
                token: '000-nocheck-now-000'
              });
            })
            
          } else {
            return res.json({
              status: false,
              message: "Password is not matched."
            });
          }
        });

        
      }else{
        return res.json({
          status: false,
          message: "Email is not existed."
        });
      }
      
    })
  } else {
    return res.json({
      status: false,
      message: "Email and password are required."
    });
  }    
});




module.exports = router;