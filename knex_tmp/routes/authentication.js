var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    bcrypt = require('bcrypt-nodejs'),
    tbl = 'pos_account',
    jwt = require('jsonwebtoken');

router.post('/signin', async function(req, res) {
  if (req.body.email && req.body.pass ) {
    var acc = await knex('pos_account').select().where({'email': req.body.email})
    if (acc.length != 0) {
        var emp = await knex('pos_employee').select('pos_employee.store', 'pos_role.name as role_name')
        .join('pos_store', {'pos_employee.store': 'pos_store.uuid'})
        .join('pos_role', {'pos_employee.role': 'pos_role.uuid'})
        .where({'pos_employee.uuid': acc[0].employee});

        var devicesAvail = await knex('pos_device').select('uuid', 'name').where({'store': emp[0].store});

        var schedulesAvail = await knex('pos_schedule').select('uuid', 'code', 'name', 'begin', 'end')
        .where({'store': emp[0].store});

        bcrypt.compare(req.body.pass, acc[0].pass, function(err, matched) {
            if (matched) {
                var user = {
                    account_id: acc[0].uuid,
                    employee_id: acc[0].employee,
                    email: acc[0].email,
                    display_name: acc[0].display_name,
                    store_id: emp[0].store,
                    role: emp[0].role_name,
                    devices: devicesAvail,
                    schedules: schedulesAvail
                }
                jwt.sign({emai: acc[0].email, store: emp[0].store}, 'secretkey', { expiresIn: '1h' }, (err, token) => {
                    if (err) {
                        return res.send(400, 'Sorry we cannot send you token. Try again after 5m')
                    }
                    return res.json({
                        _token: token,
                        _data: user
                    });
                });
            }
            else {
                return res.json({
                    status: false,
                    message: "Your password is wrong!"
                });
            }
        });
    } else {
        return res.json({
            status: false,
            message: "Your email is not existing!"
        });
    }
  } else {
    return res.json({
      status: false,
      message: "Please provide username and password"
    });
  }    
});

module.exports = router;