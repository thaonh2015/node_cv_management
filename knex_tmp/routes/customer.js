var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_customer';

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
      name: req.body.name
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    name: req.body.name
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

// other stuff
router.post('/filter', function(req, res) {
  if (req.body.keyword !== '') {
    knex('pos_customer').select()
    .where('name', 'like', '%'+req.body.keyword+'%')
    .orWhere('qrcode', '=', req.body.keyword)
    .orWhere('phone', '=', req.body.keyword)
    .then(rs => {
      return res.json({
        status: true,
        result: rs
      });
    }).catch(err => {
      return res.json({
        status: false,
        message: "Search customer fail. Try again"
      });
    })
  }else {
    knex('pos_customer').select().then(rs => {
      return res.json({
        status: true,
        result: rs
      });
    }).catch(err => {
      return res.json({
        status: false,
        message: "Search customer fail. Try again"
      });
    })
   
  }
});

module.exports = router;