var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_bill'
    _ = require('lodash');

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
      session: req.body.session,
      customer: req.body.customer,
      code: req.body.code
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    session: req.body.session,
      customer: req.body.customer,
      code: req.body.code
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

//other stuff

router.post('/place-order', async function(req, res) {
  if (req.body.session_id && req.body.customer_id && req.body.details && req.body.details.length > 0) {
    //console.log('data submitted {}', req.body.details);

    var sess = await knex('pos_session').select('*').where({uuid: req.body.session_id })
    .then(data => {
      console.log('check valid session {}', data)
      return data;
    })

    if (sess.length == 0) {
      return res.json({
        status: false,
        message: "Session is not existed"
      });
    }

    if (sess[0].end != undefined) {
      return res.json({
        status: false,
        message: "Session is closed already at " + sess[0].end
      });
    }

    var orderNo = _.now();

    knex.transaction(function(trx) {
    
      return trx
        .insert({
          code: orderNo,
          session: req.body.session_id,
          customer: req.body.customer_id,
          order_type: req.body.order_type? req.body.order_type :'IN_PLACED',
          payment_type: req.body.payment? req.body.payment: 'CASH',
          subtotal: req.body.subtotal,
          tax: req.body.tax,
          discount: req.body.discount,
          total: req.body.total,
          paid: req.body.paid,
          change: req.body.change,
          status: req.body.order_type == 'IN_PLACED'? 'WAITING': 'COMPLETED',
          seat: req.body.seat_id?req.body.seat_id:null
        }, 'uuid')
        .into('pos_bill')
        .then(function(ids) {
          var promises = req.body.details.map(function(detail){
            detail.bill = ids[0];
            // Some validation could take place here.
            return trx.insert(detail).into('pos_bill_line');
          })
          return Promise.all(promises);
        });
    }).then(function(inserts) {
      console.log(inserts + ' new details line saved.');
      return res.json({
        status: true,
        result: {
          message: "Bill " + orderNo + " is place successfully."
        }
        
      });
    })
    .catch(function(error) {
      console.error(error);
      return res.json({
        status: false,
        message: "Fail to place order. Try again!"
      });
    });
    
  } else {
    return res.json({
      status: false,
      message: "Session, Customer and Bill details are required."
    });
  }
});

router.get('/:id/view', async function(req, res) {
  console.log('view order')

  var bill = await knex('pos_bill').select('pos_bill.*', 'pos_customer.name as customer_name', 'pos_seat.code as seat_code')
  .join('pos_customer', {'pos_bill.customer': 'pos_customer.uuid'})
  .join('pos_seat', {'pos_bill.seat': 'pos_seat.uuid'})
  .where({'pos_bill.uuid': req.params.id})
  .then(rs => {
    return rs[0];
  })

  var details = await knex('pos_bill_line').select('pos_bill_line.*', 
  'pos_product.name as product_name',  
  'pos_product_variant.name as variant_name',
  'pos_product_specification.name as specification_name')
  .join('pos_product', {'pos_bill_line.product': 'pos_product.uuid'})
  .join('pos_product_variant', {'pos_bill_line.variant': 'pos_product_variant.uuid'})
  .leftJoin('pos_product_specification', {'pos_bill_line.specification': 'pos_product_specification.uuid'})
  .where({'pos_bill_line.bill': req.params.id})
  .then(function(data){
      return data;
  });

  bill.details = details;

  return res.json({
    status: true,
    result: bill
  });
});

router.post('/current', async function(req, res) {
  if (req.body.employee_id && req.body.status) {
    knex('pos_bill').select('pos_bill.*', 'pos_seat.code as seat_code')
    .join('pos_session', {'pos_bill.session': 'pos_session.uuid'})
    .join('pos_seat', {'pos_bill.seat': 'pos_seat.uuid'})
    .where({'pos_bill.status':req.body.status})
    .andWhere({
      'pos_session.employee': req.body.employee_id
    })
    .then(rs => {
      return res.json({
        status: true,
        result: rs.map(item => {
          return {
            bill_uuid: item.uuid,
            code: item.code,
            status: item.status,
            type: item.order_type,
            payment: item.payment_type,
            total: item.total,
            seat: item.seat_code
          }
        })
      });
    }).catch(err => {
      console.log('err fetch current bill waiting {}',err)
      return res.json({
        status: false,
        message: "Fetch current bill fail."
      });
    })
    
  } else {
    return res.json({
      status: false,
      message: "Employee id and status bill are required."
    });
  }    
});

router.post('/complete', async function(req, res) {
    if (req.body.code) {
      
      var completed = await knex('pos_bill').select('status').where({code: req.body.code })
      .then(data => {
        return data;
      })

      if (completed.length == 0) {
        return res.json({
          status: false,
          message: "Bill is not exist"
        });
      }
  
      if (completed[0].status !== 'WAITING') {
        return res.json({
          status: false,
          message: "Bill is marked either COMPLETED or CANCELED"
        });
      }
      knex('pos_bill').update({
        status: 'COMPLETED'
      }).where({code: req.body.code})
      .then(data => {
        return res.json({
          status: true,
          result: {
            completed_bill_id: req.body.code
          }
          
        });
      })
      .catch(err => {
        return res.json({
          status: false,
          message: "Sorry, you cannot complete this bill. Try again."
        });
      })

    }else {
      return res.json({
        status: false,
        message: "Bill code is required to complete."
      });
    }
});


module.exports = router;