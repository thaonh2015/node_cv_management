var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_coupon',
    _ = require('lodash');

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
      store: req.body.store,
      code: req.body.code,
      name: req.body.name
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    store: req.body.store,
    code: req.body.code,
    name: req.body.name
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

// other stuff 

router.post('/get-by-store', async function(req, res) {
  if (req.body.store_id) {
    var coupons = await knex('pos_coupon').select().where({store: req.body.store_id})
    .then(function(data){
        return data;
    });

   for (var i=0; i< coupons.length; i++){
      if (coupons[i].type === 'SINGLE_PRODUCT') {
        var products = await knex('pos_coupon_product')
        .select('pos_coupon_product.product', 'pos_coupon_product.net_discount', 'pos_coupon_product.percent_discount')
        .join('pos_coupon', {'pos_coupon_product.coupon': 'pos_coupon.uuid'})
        .where({'pos_coupon.uuid': coupons[i].uuid})
        .then(function(data){
            console.log('products {}', data)
            return data;
            
        });
        coupons[i].products = products;
        coupons[i].cart = null;
      }else {
        var cart = await knex('pos_coupon_cart')
        .select('pos_coupon_cart.net_discount', 'pos_coupon_cart.percent_discount')
        .join('pos_coupon', {'pos_coupon_cart.coupon': 'pos_coupon.uuid'})
        .where({'pos_coupon.uuid': coupons[i].uuid})
        .then(function(data){
            console.log('cart {}', data)
            return data;
        });
        coupons[i].products = null;
        coupons[i].cart = cart;
        
      }
    }

  
    return res.json({
      status: true,
      result: coupons
    });
    
  } else {
    return res.json({
      status: false,
      message: "Store id is required."
    });
  }  
});

module.exports = router;