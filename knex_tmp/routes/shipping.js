var express = require('express'),
    router = express.Router(),
    knex = require('../db'),
    CRUD = require('../models/generic.model'),
    tbl = 'pos_shipping',
    _ = require('lodash');

router.get('/', function(req, res) {
  CRUD.getAll(tbl).then(function(data){
      res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.post('/', function(req, res) {
  CRUD.create(tbl, {
      store: req.body.store,
      code: req.body.code,
      name: req.body.name
    })
    .then(function(data){
        res.json(data);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.get('/:id', function(req, res) {
  CRUD.get(tbl, req.params.id)
    .then(function(data){
        res.json(data[0]);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.put('/:id', function(req, res) {
  CRUD.update(tbl, req.params.id, {
    store: req.body.store,
    code: req.body.code,
    name: req.body.name
  }).then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

router.delete('/:id', function(req, res){
  CRUD.delete(tbl, req.params.id)
  .then(function(data){
        res.sendStatus(200);
    }).catch(function(err){
      console.error(err);
      res.status(500).send(err.sqlMessage);
    });
  });

// other stuff 

router.post('/get-by-store', async function(req, res) {
  if (req.body.store_id) {
    var ship = await knex('pos_shipping').select().where({store: req.body.store_id})
    .then(function(data){
        return data[0];
    });

    var ship_zone = await knex('pos_shipping_zone')
    .select('country','city')
    .where({'shipping': ship.uuid})
    .then(function(data){
        return data;
    });

    var ship_delivery = await knex('pos_shipping_delivery')
    .select('name', 'cost')
    .where({'shipping': ship.uuid})
    .then(function(data){
        return data;
    });

    var ship_quantity = await knex('pos_shipping_quantity')
    .select('base_cost', 'minimum_cost', 'item_cost')
    .where({'shipping': ship.uuid})
    .then(function(data){
        return data;
    });

    var ship_total = await knex('pos_shipping_total').select('default_cost')
    .where({'shipping': ship.uuid})
    .then(function(data){
        return data;
    });

    var ship_total_range = await knex('pos_shipping_total_range')
    .select('pos_shipping_total_range.min', 'pos_shipping_total_range.max', 'pos_shipping_total_range.cost')
    .join('pos_shipping_total', {'pos_shipping_total_range.shipping_total': 'pos_shipping_total.uuid'})
    .where({'pos_shipping_total.shipping': ship.uuid})
    .then(function(data){
      //console.log('data {}', data)
        return data;
    });

    var ship_weight = await knex('pos_shipping_weight').select('default_cost')
    .where({'shipping': ship.uuid})
    .then(function(data){
        return data;
    });

    var ship_weight_range = await knex('pos_shipping_weight_range')
    .select('pos_shipping_weight_range.min', 'pos_shipping_weight_range.max', 'pos_shipping_weight_range.cost')
    .join('pos_shipping_weight', {'pos_shipping_weight_range.shipping_weight': 'pos_shipping_weight.uuid'})
    .where({'pos_shipping_weight.shipping': ship.uuid})
    .then(function(data){
      //console.log('data {}', data)
        return data;
    });

    var ship_rate = await knex('pos_shipping_flat_rate').select('base_cost')
    .where({'shipping': ship.uuid})
    .then(function(data){
        return data;
    });

    var ship_rate_rule = await knex('pos_shipping_flat_rate_rule')
    .select('pos_shipping_flat_rate_rule.city','pos_shipping_flat_rate_rule.district', 'pos_shipping_flat_rate_rule.cost')
    .join('pos_shipping_flat_rate', {'pos_shipping_flat_rate_rule.flat_rate': 'pos_shipping_flat_rate.uuid'})
    .where({'pos_shipping_flat_rate.shipping': ship.uuid})
    .then(function(data){
      //console.log('data {}', data)
        return data;
    });
   
    // tax_bases.forEach(tax_base => {
    //   tax_base.details = _.filter(tax_base_detail, {tax_base: tax_base.uuid});
    //   tax_base.details = tax_base.details.map(item => _.omit(item, ['tax_base','uuid']));
      
    // });

    ship.available_zones = ship_zone;
    ship.ship_delivery = ship_delivery;
    ship.ORDER_QUANTITY = ship_quantity[0];
    ship.ORDER_TOTAL = ship_total[0];
    ship.ORDER_TOTAL.ranges = ship_total_range;
    ship.ORDER_WEIGHT = ship_weight[0];
    ship.ORDER_WEIGHT.ranges = ship_weight_range;
    ship.FLAT_RATE = ship_rate[0];
    ship.FLAT_RATE.destinations = ship_rate_rule;

    return res.json({
      status: true,
      result: ship
    });
    
  } else {
    return res.json({
      status: false,
      message: "Store id is required."
    });
  }  
});

module.exports = router;