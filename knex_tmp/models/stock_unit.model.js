const knex = require('../db'),
    tbl = 'pos_stock_unit';

exports.tblName = tbl;

exports.getAll = () => {  
    return knex(tbl).select().orderBy('uuid');
};

exports.create = (payload) => {  
    return knex(tbl).insert(payload).returning('*');
};

exports.get = (id) => {
    return knex(tbl).select().where({uuid: id})
};

exports.update = (id, payload) => {  
    return knex(tbl).update(payload).where({uuid: id});
};

exports.delete = (id) => {  
    return knex(tbl).delete().where({uuid: id})
};