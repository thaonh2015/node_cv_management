const knex = require('../db');

exports.getAll = (tbl) => {  
    return knex(tbl).select().orderBy('uuid');
};

exports.create = (tbl, payload) => {  
    return knex(tbl).insert(payload).returning('*');
};

exports.get = (tbl, id) => {
    return knex(tbl).select().where({uuid: id})
};

exports.update = (tbl, id, payload) => {  
    return knex(tbl).update(payload).where({uuid: id});
};

exports.delete = (tbl, id) => {  
    return knex(tbl).delete().where({uuid: id})
};