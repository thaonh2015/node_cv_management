const express = require('express');
const path = require('path');
const session = require('express-session');
//const MongoStore = require('connect-mongo')(session);
const auth = require('./routes/auth');

const config = require('./config.dev')

const app = new express();

const mongoose = require('mongoose');
//mongoose.connect('mongodb://172.22.0.2/visg_hunter_nodejs');
mongoose.connect(`mongodb://${config.url_mongo}/${config.schema_name}`);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//use sessions for tracking logins
app.use(session({
    secret: 'work++hard',
    resave: true,
    saveUninitialized: false,
    maxAge: 24 * 60 * 60 * 1000
    // store: new MongoStore({
    //   mongooseConnection: db
    // }),
    //cookie: { maxAge: 60000 * 30 } // 20 minutes
  }));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
//     secret: 'keyboard++cat'
// }))
app.use(require('connect-flash')());

const ENV = process.env.ENV || 'PROD';
const User = require('./models/user');
const Employee = require('./models/employee');

/*
if (ENV === 'dev') {
    
    var the_first = false;
    app.use(function(req, res, next){
        if (the_first) {
            next()
        }
        else {
            console.log('mock....dev')
            User.authenticate(config.account.username, config.account.password, async function (err, user) {
                req.session.loggedIn = user;
            
                var user_roles = [];
                user.roles.forEach(item => {
                    user_roles.push(item.role_name);
                }) 
                req.session.user_roles = user_roles;

                the_first = true
                next();
            })
        }
        
    });
} 
*/

const fileUpload = require('express-fileupload');
app.use(fileUpload());

app.use(function(req, res, next){
    res.locals.info = req.flash('info');
    res.locals.loggedIn = req.session.loggedIn;
    //res.locals.user_roles = req.session.user_roles;
    //res.locals.occupations = req.session.occupations;
    res.locals.isManagerCommon = (res.locals.loggedIn && res.locals.loggedIn.user_roles.indexOf('super_admin') > -1) || (res.locals.loggedIn && res.locals.loggedIn.user_roles.indexOf('admin') > -1) || (res.locals.loggedIn && res.locals.loggedIn.user_roles.indexOf('manager') > -1);
    res.locals.isSuperAdmin = (res.locals.loggedIn && res.locals.loggedIn.user_roles.indexOf('super_admin') > -1);
    res.locals.isAdmin = (res.locals.loggedIn && res.locals.loggedIn.user_roles.indexOf('admin') > -1);
    res.locals.isManager = (res.locals.loggedIn && res.locals.loggedIn.user_roles.indexOf('manager') > -1);
    
    // res.locals.hasRoles = (roles) => {
    //     for (var i = 0; i< roles.length; i++) {
    //         if (res.locals.user_roles.indexOf(roles[i])> -1){
    //             return true;
    //         }
    //     }
    //     return false;
    // }
    next();
});

app.use('/', require('./routes/index_route'));

app.use('/cities', auth.checkSignIn, require('./routes/city_route'));
app.use('/occupations', auth.checkSignIn, require('./routes/occupation_route'));
app.use('/qualifications', auth.checkSignIn, require('./routes/qualification_route'));
app.use('/experiences', auth.checkSignIn, require('./routes/experience_route'));
app.use('/positions', auth.checkSignIn, require('./routes/position_route'));
app.use('/jobtypes', auth.checkSignIn, require('./routes/jobtype_route'));
app.use('/languages', auth.checkSignIn, require('./routes/language_route'));

app.use('/candidates', auth.checkSignIn, require('./routes/candidate_route'));

app.use('/employees', auth.checkSignIn, require('./routes/employee_route'));

app.use('/permissions', auth.checkSignIn, require('./routes/permission_route'));
app.use('/users', auth.checkSignIn, require('./routes/user_route'));
// app.use('/candidates', auth.checkSignIn, require('./routes/candidate_route'));

var cache = require('./routes/cache');

app.listen(config.port, () => {
    //if (config.production) {
        cache.emptyAll();
    //}
    
    console.log(`Server start on port ${config.port}...`);
})