var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ContractSchema = Schema({
  //nganh nghe kinh doanh
  customer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Customer'
  },
  contact_legal: {type: String, required: true, maxlength: 200},
  contact_position: {type: String, required: true, maxlength: 200},
  contact_phone: {type: String, maxlength: 10},
  contact_email: {type: String, maxlength: 200},

  signed_employee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employee'
  },

  signed_date: {type: Date, default: Date.now},
  begin_date: {type: Date, default: Date.now},
  end_date: {type: Date},

  enclosed_file: {type: String, maxlength: 200},

  contract_status: {type: String, 
    enum: ['Preparation', 'Submitted', 'Signed', 'Done', 'Broken'], 
    default: 'Preparation'},
  notes: {type: String},
  recruitments: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Recruitment'
  }],
  
  createdAt: {type: Date, default: Date.now},
  modifiedAt: {type: Date},
  
});

var Contracts = mongoose.model('Contract', ContractSchema);
module.exports = Contract;