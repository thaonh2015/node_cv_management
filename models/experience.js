var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ExperienceSchema = Schema({
    name: {type: String, required: true, maxlength: 200},
    ordinal: {type: Number, required: true},
    createdAt: {type: Date, default: Date.now}
});

var Experience = mongoose.model('Experience', ExperienceSchema);
module.exports = Experience;