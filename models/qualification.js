var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var QualificationSchema = Schema({
    name: {type: String, required: true, maxlength: 200},
    ordinal: {type: Number, required: true},
    createdAt: {type: Date, default: Date.now}
});

var Qualification = mongoose.model('Qualification', QualificationSchema);
module.exports = Qualification;