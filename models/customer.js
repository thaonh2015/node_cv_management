var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CustomerSchema = Schema({
  //nganh nghe kinh doanh
  occupations: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Occupation'
  }],
  name: {type: String, required: true, maxlength: 200},
  tax_code: {type: String, maxlength: 20},
  address: {type: String, required: true, maxlength: 200},
  phone: {type: String, required: true, trim: true, maxlength: 20},
  website: {type: String, maxlength: 200},
  notes: {type: String},
  
  createdAt: {type: Date, default: Date.now},
  modifiedAt: {type: Date},
  
});

var Customer = mongoose.model('Customer', CustomerSchema);
module.exports = Customer;