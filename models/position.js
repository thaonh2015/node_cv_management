var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PositionSchema = Schema({
    name: {type: String, required: true, maxlength: 200},
    ordinal: {type: Number, required: true},
    createdAt: {type: Date, default: new Date()}
});

var Position = mongoose.model('Position', PositionSchema);
module.exports = Position;