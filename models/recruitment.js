var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var RecruitmentSchema = Schema({
  //nganh nghe kinh doanh
  position: {type: String, require: true, maxlength: 200},
  quantity: {type: Number, require: true, default: 1},
  recruit_from: {type: Date, required: true, default: Date.now},
  recruit_to: {type: Date, required: true, default: Date.now},
  salary_min: {type: Number, required: true, default: 0},
  salary_max: {type: Number, required: true, default: 0},
  salary_currency: {type: String, enum: ['$', 'VND'], default: 'VND'},
  working_place: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City'
  },
  recruitment_status: {type: String, 
    enum: ['InProgress', 'Completed', 'Uncompleted'], 
    default: 'InProgress'},
  other_requirement: {type: String},
  // will be payment
//   passed_candidates: [{
//       candidate: {
//         type: mongoose.Schema.Types.ObjectId,
//         ref: 'Candidate'
//       },
//       status: {type: String, enum: ['DuringInsurance','CompletedInsurance','Insuranced']}
//   }],
  createdAt: {type: Date, default: Date.now},
  modifiedAt: {type: Date},
  
});

var Recruitment = mongoose.model('Recruitment', RecruitmentSchema);
module.exports = Recruitment;