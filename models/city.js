var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var CitySchema = Schema({
  name: {type: String, required: true, maxlength: 200},
  ordinal: {type: Number, required: true},
  createdAt: {type: Date, default: new Date()}
});

module.exports = mongoose.model('City', CitySchema);