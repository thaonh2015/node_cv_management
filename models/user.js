var mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

var Schema = mongoose.Schema;

var UserSchema = Schema({
  name: {type: String, required: true},
  email: {type: String, 
    unique: true,
    required: true,
    trim: true},
  password: {type: String, required: true},
  active: {type: Boolean, default: false},
  createdAt: {type: Date, default: new Date()},
  roles: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role'
  }],
  employee: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employee'
  }
});

//authenticate input against database
UserSchema.statics.authenticate = function (email, password, callback) {
  //console.log('aaaaa1');
  User.findOne({ email: email }).populate('roles')
    .exec(function (err, user) {
      //console.log(user)
      if (err) {
        return callback(err)
      } else if (!user) {
        var err = new Error('User not found.');
        err.status = 401;
        return callback(err);
      }
      //console.log('aaaaa');
      bcrypt.compare(password, user.password, function (err, result) {
        if (result === false) {
          var err = new Error('Password is incorrect.');
          err.status = 401;
          return callback(err);
        } else if (user.active) {
          var user_roles = [];
          user.roles.forEach(item => {
              user_roles.push(item.role_name);
          }) 
          let account = {
            _id: user._id,
            name: user.name,
            email: user.email,
            employee: user.employee,
            user_roles: user_roles
          }
          
          return callback(null, account);
        } else {
          var err = new Error('User is not active yet.');
          err.status = 401;
          return callback(err);
        }
      })
      
    });
}

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
  var user = this;
  bcrypt.hash(user.password, 10, function (err, hash) {
    if (err) {
      return next(err);
    }
    user.password = hash;
    next();
  })
});
var User = mongoose.model('User', UserSchema);
module.exports = User;