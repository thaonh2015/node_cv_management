var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var EmployeeSchema = Schema({
  //nganh nghe tuong tac
  occupations: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Occupation'
  }],
  // manager: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'Employee'
  // },
  // account: {
  //   type: mongoose.Schema.Types.ObjectId,
  //   ref: 'User'
  // },
  // name: {type: String, required: true, maxlength: 200},
  position: {type: String, required: true, maxlength: 200},
  // urgent_contact: {type: String, maxlength: 200},
  // urgent_contact_phone: {type: String, maxlength: 20},
  // urgent_contact_relationship: {type: String, 
  //   enum: ['Husband/Wife', 'Father/Mother', 'Brother/Sister', 'Son/Daughter', 'Friends'], 
  //   default: 'Husband/Wife'},
  // employment_status: {type: String, 
  //   enum: ['Internship', 'Hired', 'Resigned'], 
  //   default: 'Hired'},
  // contract_date: {type: Date, default: Date.now },
  // contract_type: {type: String, 
  //   enum: ['Temporary', '3Month', '1Year', '3Year', 'LongYear'], 
  //   default: '1Year'},
  dob: {type: Date, required: true, default: Date.now},
  gender: {type: Boolean, required: true, default: true},
  marital: {type: String, maxlength: 1, default: 'S'},
  // no_children: {type: Number, default: 0},
  // national_id: {type: String, maxlength: 20},
  // passport_no: {type: String, maxlength: 20},
  // passport_expired: {type: Date, default: Date.now},
  // social_code: {type: String, maxlength: 20},
  // tax_code: {type: String, maxlength: 20},
  address: {type: String, required: true, maxlength: 200},
  phone_personal: {type: String, maxlength: 20},
  phone_company: {type: String, maxlength: 20},
  //phone_office: {type: String, maxlength: 20},
  notes: {type: String},
  
  createdAt: {type: Date, default: Date.now},
  modifiedAt: {type: Date},
  
});

var Employee = mongoose.model('Employee', EmployeeSchema);
module.exports = Employee;