var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var TaskSchema = Schema({
    recruitment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Recruitment'
      },
    consultant: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    },
    actions: [
        {
            candidate: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Employee'
            },
            tracking: [{
                status: {type: String, enum: ['Called/Emailed','HTS-Interviewed','Customer-Interviewed', 'Done-Offered', 'Failure']},
                perform_date: {type: Date, default: Date.now}
            }],
            failure_reason: {type: String} 
        }
    ],
    createdAt: {type: Date, default: Date.now},
    modifiedAt: {type: Date},
  
});

var Task = mongoose.model('Task', TaskSchema);
module.exports = Task;