var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var JobTypeSchema = Schema({
    name: {type: String, required: true, maxlength: 200},
    ordinal: {type: Number, required: true},
    createdAt: {type: Date, default: Date.now}
});

var JobType = mongoose.model('JobType', JobTypeSchema);
module.exports = JobType;