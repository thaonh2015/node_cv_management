var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PaymentSchema = Schema({
    recruitment: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Recruitment'
    },
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer'
      },
    consultant: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    },
    candidate: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    },
    started_date: {type: Date, default: Date.now},
    gross_offered: {type: Number, default: 0},
    insurance: {type: String, enum: ['30days','90days'], default:'30days'},
    acceptanced_date: {type: Date},
    invoice_date: {type: Date},
    payment_status: {type: String, enum: ['Paid', 'NotYet', 'LatePaid', 'Unpaid'], default: 'Paid'},
    candidate_status: {type: String, enum: ['Probation', 'Passed', 'Canceled']}, 

    revenue: {type: Number, default: 0},
    vat: {type: Number, default: 0},
    vat_revenue: {type: Number, default: 0}, // revenure * vat%

    service: {type: String, enum: ['Normal', 'Insurance']},
    //insurane
    replacement: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Payment'
    },
    diff_offered: {type: Number, default: 0},
    addition: {type: Number, default: 0},
    vat_addition: {type: Number, default: 0},
    debit_amount: {type: Number, default: 0},


    createdAt: {type: Date, default: Date.now},
    modifiedAt: {type: Date},
  
});

var Payment = mongoose.model('Payment', PaymentSchema);
module.exports = Payment;