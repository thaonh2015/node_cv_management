var mongoose = require('mongoose');

var Schema = mongoose.Schema;

const CandidateStatus = Object.freeze({
  Active: 'Active',
  Inactive: 'Inactive',
  Draft: 'Draft',
});

var CandidateSchema = Schema({
  //nganh nghe
  occupations: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Occupation',
    required: true
  }],
  //dia diem lam viec
  cities: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'City',
    required: true
  }],
  name: {type: String, required: true, maxlength: 200},
  //chuc danh cong viec
  work_title: {type: String, maxlength: 200},
  email: {type: String, required: true, trim: true, maxlength: 100, unique: true},
  phone: {type: String, required: true, trim: true, maxlength: 20},
  gender: {type: Boolean, required: true, default: true},
  available_time: {type: Date, required: true},
  dob: {type: Date, required: true},
  qualification: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Qualification',
    required: true
  },
  experience: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Experience',
    required: true
  },
  position: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Position',
    required: true
  },
  languages: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Language',
    required: true
  }],
  job_types: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'JobType',
    required: true
  }],
  salary_current: {type: Number, default: -1},
  salary_min: {type: Number, default: -1},
  salary_max: {type: Number, default: -1},
  current_currency: {type: String, enum: ['$', 'VND'], default: 'VND'},
  salary_currency: {type: String, enum: ['$', 'VND'], default: 'VND'},
  submitedDate: {type: Date, default: new Date()},
  resume_url: {type: String, maxlength: 200},
  photo_url: {type: String, maxlength: 200},
  short_education: {type: String, maxlength: 2000},
  short_experience: {type: String, maxlength: 2000},
  additional: {type: String, maxlength: 2000},
  can_code: {type: String, maxlength: 30, unique: true},
  // status: {
  //   type: String,
  //   enum :  Object.values(CandidateStatus),
  //   default: CandidateStatus.Active,
  //   require: true
  // },
  marital: {type: String, maxlength: 1, default: 'S'},
  short_address: {type: String, maxlength: 200},
  nationality: {type: String, maxlength: 20},
  createdAt: {type: Date, default: new Date()},
  //modifiedAt: {type: Date, default: new Date()},
  employee: {type: String, maxlength: 100},
  work_experience: [
    {
      from_date: {type: Date, default: new Date()},
      to_date: {type: Date, default: new Date()},
      company: {type: String, maxlength: 200},
      position_level: {type: String, maxlength: 200},
      main_duties: {type: String, maxlength: 2000}
    }
  ],
  notes: [
    {
      modifiedAt: {type: Date, default: Date.now},
      consultant: {type: String, maxlength: 200},
      content: {type: String, maxlength: 2000}
    }
  ]
});

var Candidate = mongoose.model('Candidate', CandidateSchema);
module.exports = Candidate;