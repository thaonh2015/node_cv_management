var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var RoleSchema = Schema({
  role_name: {type: String, 
    unique: true,
    required: true,
    trim: true},
  createdAt: {type: Date, default: new Date()},
  permissions_acl: [String]

});

var Role = mongoose.model('Role', RoleSchema);
module.exports = Role;