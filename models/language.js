var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var LanguageSchema = Schema({
    name: {type: String, required: true, maxlength: 200},
    ordinal: {type: Number, required: true},
    createdAt: {type: Date, default: Date.now}
});

var Language = mongoose.model('Language', LanguageSchema);
module.exports = Language;