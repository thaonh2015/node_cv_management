var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PermissionSchema = Schema({
  permission_name: {type: String, 
    unique: true,
    required: true,
    trim: true},
  createdAt: {type: Date, default: new Date()}
});

var Permission = mongoose.model('Permission', PermissionSchema);
module.exports = Permission;