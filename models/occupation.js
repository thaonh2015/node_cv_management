var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var OccupationSchema = Schema({
    name: {type: String, required: true, maxlength: 200},
    group: {type: String, maxlength: 200},
    ordinal: {type: Number, required: true},
    createdAt: {type: Date, default: new Date()}
});

var Occupation = mongoose.model('Occupation', OccupationSchema);
module.exports = Occupation;