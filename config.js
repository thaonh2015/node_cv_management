const config = {
    port: process.env.PORT || 9130,
    url_mongo: process.env.URL_MONGO || 'localhost',
    schema_name: process.env.SCHEMA_DB || 'htalentsearch',
    url_redis: process.env.URL_REDIS || 'localhost',
    port_redis: process.env.PORT_REDIS || 6379,
    production: true
}

module.exports = config;