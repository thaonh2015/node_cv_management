const config = {
    port: process.env.PORT || 3001,
    url_mongo: process.env.URL_MONGO || 'localhost',
    schema_name: process.env.SCHEMA_DB || 'htalentsearch',
    url_redis: process.env.URL_REDIS || 'localhost',
    port_redis: process.env.PORT_REDIS || 6379,
    production: process.env.PROD || false,
    account: {
        username: 'hoangthao.ng@gmail.com',
        password: '123456'
    },
    account_admin: {
        username: 'admin@mail.com',
        password: '111111'
    },
    account_manager: {
        username: 'manager@mail.com',
        password: '111111'
    },
    account_employee: {
        username: 'consult@mail.com',
        password: '123123'
    },
}

module.exports = config;