#! /usr/bin/env node

console.log('This script populates a some test books, authors, genres and bookinstances to your database. Specified database as argument - e.g.: populatedb mongodb://your_username:your_password@your_dabase_url');

//Get arguments passed on command line
var userArgs = process.argv.slice(2);
if (!userArgs[0].startsWith('mongodb://')) {
    console.log('ERROR: Please use $node migrationdb mongodb://localhost:27017/[schema_name]> ');
    return
}

var async = require('async')
var Role = require('./models/role')
var User = require('./models/user')
var City = require('./models/city')
var Occupation = require('./models/occupation')
var Position = require('./models/position')
var Experience = require('./models/experience')
var Qualification = require('./models/qualification')
var Language = require('./models/language')
var JobType = require('./models/jobtype')


var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB);
var db = mongoose.connection;
mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));

var roles = [];
var users = [];

function createRole(uuid, role_name, cb) {
    var detail = { role_name: role_name };
    if (uuid != null) {
        detail._id = mongoose.Types.ObjectId(uuid);
    }

    var role = new Role(detail);

    role.save(function (err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New Role: ' + role_name);
        roles.push(role)
        cb(null, role)
    });
}

function createUser(uuid, name, email, password, roles, cb) {
    var detail = { name: name, email: email, password: password,
        roles: roles, active: true };
    if (uuid != null) {
        detail._id = mongoose.Types.ObjectId(uuid);
    }
    var user = new User(detail);

    user.save(function (err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New user: ' + email);
        users.push(user)
        cb(null, user)
    });
}

function createCity(cities_name, cb) {
    var cities = [];
    for (var i=0; i< cities_name.length; i++) {
        cities.push(new City({name: cities_name[i], ordinal: i}))
    }
    City.create(cities, function(err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New cities: ' + cities);
        cb(null, cities)
    })
   
}

function createPosition(names, cb) {
    var objs = [];
    for (var i=0; i< names.length; i++) {
        objs.push(new Position({name: names[i], ordinal: i}))
    }
    Position.create(objs, function(err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New positions: ' + objs);
        cb(null, objs)
    })
   
}

function createExperience(names, cb) {
    var objs = [];
    for (var i=0; i< names.length; i++) {
        objs.push(new Experience({name: names[i], ordinal: i}))
    }
    Experience.create(objs, function(err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New experiences: ' + objs);
        cb(null, objs)
    })
   
}

function createQualification(names, cb) {
    var objs = [];
    for (var i=0; i< names.length; i++) {
        objs.push(new Qualification({name: names[i], ordinal: i}))
    }
    Qualification.create(objs, function(err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New Qualifications: ' + objs);
        cb(null, objs)
    })
   
}

function createLanguage(names, cb) {
    var objs = [];
    for (var i=0; i< names.length; i++) {
        objs.push(new Language({name: names[i], ordinal: i}))
    }
    Language.create(objs, function(err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New Languages: ' + objs);
        cb(null, objs)
    })
   
}

function createJobType(names, cb) {
    var objs = [];
    for (var i=0; i< names.length; i++) {
        objs.push(new JobType({name: names[i], ordinal: i}))
    }
    JobType.create(objs, function(err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New JobTypes: ' + objs);
        cb(null, objs)
    })
   
}

function createOccupation(occupations, cb) {
    var objs = [];
    for (var i=0; i< occupations.length; i++) {
        objs.push(new Occupation({group: occupations[i].group, name: occupations[i].name, ordinal: i}))
    }
    Occupation.create(objs, function(err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New occupations: ' + objs);
        cb(null, objs)
    })
   
}

function createDemoRoles(cb){
    async.parallel([
        function(callback) {
            createRole('5bc02c75876e1b3efa434001', 'super_admin', callback);
        },
        function(callback) {
            createRole(null, 'admin', callback);
        },
        function(callback) {
            createRole(null, 'manager', callback);
        },
        function(callback) {
            createRole(null, 'employee', callback);
        },
    ],cb);
}

function createDemoUsers(cb){
    async.parallel([
        function(callback) {
            createUser('5bc02c75876e1b3efa434000', 'Thao Nguyen', 'hoangthao.ng@gmail.com', '123123', roles, callback);
        },
        function(callback) {
            var r = roles.filter(item => {
                return item.role_name === 'admin' || item.role_name === 'manager' 
            })
            createUser(null, 'Mr Admin', 'admin@htalent-search.vn', '123456', r, callback);
        },
       
    ],cb);
}

function createDemoPositions(cb){
    async.parallel([
        function(callback) {
            createPosition(['Sinh viên/Thực tập sinh',
            'Mới tốt nghiệp',
        'Nhân viên/ Chuyên viên',
    'Trưởng nhóm/ Giám sát','Quản lý','Giám đốc', 'Tổng giám đốc/Điều hành cấp cao'], callback);
        },
        
       
    ],cb);
}

function createDemoExperiences(cb){
    async.parallel([
        function(callback) {
            createExperience(['Chưa có kinh nghiệm',
        'Dưới 1 năm', 'Từ 1 đến 3 năm', 'Từ 3 đến 5 năm', 'Từ 5 đến 7 năm', 'Từ 7 đến 10 năm', 'Trên 10 năm'], callback);
        },
    ],cb);
}

function createDemoQualifications(cb){
    async.parallel([
        function(callback) {
            createQualification(['Chưa tốt nghiệp', 'Trung học', 'Trung cấp', 'Cao Đẳng', 'Đại học', 'Sau Đại học', 'Khác'], callback);
        },
    ],cb);
}

function createDemoLanguages(cb){
    async.parallel([
        function(callback) {
            createLanguage(['Tiếng Anh', 'Tiếng Pháp', 'Tiếng Đức', 'Tiếng Nga', 'Tiếng Hoa', 'Tiếng Nhật', 'Tiếng Hàn'], callback);
        },
    ],cb);
}

function createDemoJobTypes(cb){
    async.parallel([
        function(callback) {
            createJobType(['Toàn thời gian', 'Bán thời gian', 'Thời vụ', 'Cộng tác viên', 'Thực tập'], callback);
        },
    ],cb);
}

function createDemoCities(cb){
    async.parallel([
        function(callback) {
            createCity(['TPHCM','Hanoi'], callback);
        },
    ],cb);
}

function createDemoOccupations(cb){
    async.parallel([
        function(callback) {
            createOccupation([
                {group: 'Bán hàng / Tiếp thị', name: 'Bán lẻ / Bán sỉ'},
                {group: 'Bán hàng / Tiếp thị', name: 'Tiếp thị trực tuyến'},
                {group: 'Bán hàng / Tiếp thị', name: 'Tiếp thị / Marketing'},
                {group: 'Bán hàng / Tiếp thị', name: 'Bán hàng / Kinh doanh'},
                {group: 'Dịch vụ', name: 'Phi chính phủ / Phi lợi nhuận'},
                {group: 'Dịch vụ', name: 'Vận chuyển / Giao nhận / Kho vận'},
                {group: 'Dịch vụ', name: 'Lao động phổ thông'},
                {group: 'Dịch vụ', name: 'Dịch vụ khách hàng'},
                {group: 'Dịch vụ', name: 'Tư vấn'},
                {group: 'Dịch vụ', name: 'An Ninh / Bảo Vệ'},
                {group: 'Dịch vụ', name: 'Bưu chính viễn thông'},
                {group: 'Dịch vụ', name: 'Luật / Pháp lý'},
                {group: 'Chăm sóc sức khỏe', name: 'Dược phẩm'},
                {group: 'Chăm sóc sức khỏe', name: 'Y tế / Chăm sóc sức khỏe'},
                {group: 'Hàng tiêu dùng', name: 'Thực phẩm & Đồ uống'},
                {group: 'Hàng tiêu dùng', name: 'Hàng gia dụng / Chăm sóc cá nhân'},
                {group: 'Máy tính / Công nghệ thông tin', name: 'CNTT - Phần cứng / Mạng'},
                {group: 'Máy tính / Công nghệ thông tin', name: 'CNTT - Phần mềm'},
                {group: 'Hành chánh / Nhân sự', name: 'Biên phiên dịch'},
                {group: 'Hành chánh / Nhân sự', name: 'Hành chính / Thư ký'},
                {group: 'Hành chánh / Nhân sự', name: 'Nhân sự'},
                {group: 'Hành chánh / Nhân sự', name: 'Quản lý điều hành'},
                {group: 'Kế toán / Tài chính', name: 'Bảo hiểm'},
                {group: 'Kế toán / Tài chính', name: 'Chứng khoán'},
                {group: 'Kế toán / Tài chính', name: 'Tài chính / Đầu tư'},
                {group: 'Kế toán / Tài chính', name: 'Kế toán / Kiểm toán'},
                {group: 'Kế toán / Tài chính', name: 'Ngân hàng'},
                {group: 'Truyền thông / Media', name: 'Quảng cáo / Đối ngoại / Truyền Thông'},
                {group: 'Truyền thông / Media', name: 'Giải trí'},
                {group: 'Truyền thông / Media', name: 'Mỹ thuật / Nghệ thuật / Thiết kế'},
                {group: 'Truyền thông / Media', name: 'Truyền hình / Báo chí / Biên tập'},
                {group: 'Truyền thông / Media', name: 'Tổ chức sự kiện'},
                {group: 'Kỹ thuật', name: 'Cơ khí / Ô tô / Tự động hóa'},
                {group: 'Kỹ thuật', name: 'Điện / Điện tử / Điện lạnh'},
                {group: 'Kỹ thuật', name: 'Hóa học'},
                {group: 'Kỹ thuật', name: 'Môi trường'},
                {group: 'Kỹ thuật', name: 'Dầu khí'},
                {group: 'Kỹ thuật', name: 'Khoáng sản'},
                {group: 'Kỹ thuật', name: 'Bảo trì / Sửa chữa'},
                {group: 'Giáo dục / Đào tạo', name: 'Thư viện'},
                {group: 'Giáo dục / Đào tạo', name: 'Giáo dục / Đào tạo'},
                {group: 'Khoa học', name: 'Lâm Nghiệp'},
                {group: 'Khoa học', name: 'Thủy sản / Hải sản'},
                {group: 'Khoa học', name: 'Công nghệ thực phẩm / Dinh dưỡng'},
                {group: 'Khoa học', name: 'Thống kê'},
                {group: 'Khoa học', name: 'Nông nghiệp'},
                {group: 'Khoa học', name: 'Hàng hải'},
                {group: 'Khoa học', name: 'Công nghệ sinh học'},
                {group: 'Khoa học', name: 'Trắc địa / Địa Chất'},
                {group: 'Khoa học', name: 'Thủy lợi'},
                {group: 'Khoa học', name: 'Chăn nuôi / Thú y'},
                {group: 'Sản xuất', name: 'Thu mua / Vật tư'},
                {group: 'Sản xuất', name: 'Sản xuất / Vận hành sản xuất'},
                {group: 'Sản xuất', name: 'Xuất nhập khẩu'},
                {group: 'Sản xuất', name: 'Dệt may / Da giày / Thời trang'},
                {group: 'Sản xuất', name: 'In ấn / Xuất bản'},
                {group: 'Sản xuất', name: 'An toàn lao động'},
                {group: 'Sản xuất', name: 'Quản lý chất lượng (QA/QC)'},
                {group: 'Sản xuất', name: 'Đồ gỗ'},
                {group: 'Xây dựng', name: 'Nội ngoại thất'},
                {group: 'Xây dựng', name: 'Bất động sản'},
                {group: 'Xây dựng', name: 'Kiến trúc'},
                {group: 'Xây dựng', name: 'Xây dựng'},
                {group: 'Khách sạn / Du lịch', name: 'Du lịch'},
                {group: 'Khách sạn / Du lịch', name: 'Nhà hàng / Khách sạn'},
                {group: 'Khách sạn / Du lịch', name: 'Hàng không'},
                {group: 'Nhóm ngành khác', name: 'Mới tốt nghiệp / Thực tập'},
                {group: 'Nhóm ngành khác', name: 'Ngành khác'},
            
            ], callback);
        },
        
       
    ],cb);
}

async.series([
    createDemoRoles,
    createDemoUsers,
    createDemoCities,
    createDemoOccupations,
    createDemoPositions,
    createDemoExperiences,
    createDemoQualifications,
    createDemoLanguages,
    createDemoJobTypes,
],
// optional callback
function(err, results) {
    if (err) {
        console.log('FINAL ERR: '+err);
    }
      
    console.log('FINISH!');
    //All done, disconnect from database
    mongoose.connection.close();
});