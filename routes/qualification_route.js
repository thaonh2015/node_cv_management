const express = require('express');
const router = express.Router();
const Qualification = require('../models/qualification');
const { check, validationResult } = require('express-validator/check');

const cache = require('./cache');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('Qualification');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/qualifications');
});

router.get('/', (req, res, next) => {
Qualification.find({})
.sort({createdAt: -1})
.exec(function(err, result) {
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}

res.render('qualification/list', {
title: 'Danh sách Bằng cấp',
list: result
})
});
});

router.get('/add', (req, res, next) => {
res.render('qualification/add', {
title: 'Thông tin Bằng cấp',
model: {
name: '',
ordinal: 1
}
})
});

router.post('/add',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
return res.render('qualification/add', {
title: 'Thông tin Bằng cấp',
model: req.body,
errors: errors.mapped()
})
}

let model = new Qualification();
model.name = req.body.name;
model.ordinal = req.body.ordinal;
model.save(function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
if (req.body.save_cont != undefined) {
req.flash('info', 'Them thanh cong ' + result.name)
return res.redirect('/qualifications/add');
}
res.redirect('/qualifications');
});


});

router.get('/edit/:id', (req, res, next) => {
Qualification.findById(req.params.id, function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.render('qualification/edit', {
title: 'Thông tin Bằng cấp',
model: result
})
})

});

router.post('/edit/:id',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
let model = req.body;
model._id = req.params.id;
return res.render('qualification/edit', {
title: 'Thông tin Bằng cấp',
model: model,
errors: errors.mapped()
})
}

let model = {};
model.name = req.body.name;
model.ordinal = req.body.ordinal;

Qualification.findByIdAndUpdate(req.params.id, model, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/qualifications');
});
});

router.get('/delete/:id', (req, res, next) => {
Qualification.findByIdAndDelete(req.params.id, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/qualifications');
});
});

module.exports = router;
