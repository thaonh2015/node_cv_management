const express = require('express');
const router = express.Router();
const Employee = require('../models/employee');
const User = require('../models/user');
const Occupation = require('../models/occupation');
const { check, validationResult } = require('express-validator/check');
const cache = require('./cache');
//var _async = require('async');
var mongoose = require('mongoose');
var _ = require('lodash');
var moment = require('moment');

router.get('/', (req, res, next) => {
    User.find({}).populate('employee roles')
        .sort({ name: -1 })
        .exec(function (err, result) {
            if (err) {
                console.log('error=>', err)
                return res.status(400).json({ message: err });
            }
            var rs = _.map(result, function(it) {
                var obj = it.toJSON();
                obj.role_names = _.map(it.roles, 'role_name');
                //_.omit(obj, ['roles','password']);
                //console.log(obj);
                return obj;
            })
            //console.log('rs::', rs)
            res.render('employee/list', {
                title: 'Employee list',
                list: rs
            })
        });
});

// function loadDirectory() {
//     return new Promise((resolve, reject) => {
//         _async.parallel({
//         lstOccupations: function(callback) {
//           cache.findOccupations(function(data) {
//             callback(null, data);
//           })
//         },
        
//       }, function(err, results) {
//         if (err) { 
//           console.log(err);
//           reject(err);
//         }
//         resolve(results);
//       });
//     });
    
//   }

// router.get('/add', (req, res, next) => {
//     loadDirectory().then(results => {
//         res.render('employee/add', {
//             title: 'Employee information',
//             model: {
//                 name: ''
//             },
//             lstOccupations: results.lstOccupations,
//             lstUsers: results.lstUsers
//         })
//     }).catch(err => {
//         console.log('error::', err)
//         return res.status(400).json({ message: err });
//     }) 
    
// });


router.get('/edit/:id', async function(req, res, next) {
    let dataOccupations = await Occupation.find({}).exec();
    let user = await User.findById(req.params.id).populate('employee').exec()

    let employee = {
        position: '',
        marital: 'S',
        gender: false
    };
    if (user.employee) { 
        employee = user.employee.toJSON();
        employee.dob = moment(user.employee.dob).format('DD/MM/YYYY')
    }
    
    return res.render('employee/edit', {
        title: 'Employee information',
        model: user,
        employee: employee,
        dataOccupations: dataOccupations
    })

    /*
    loadDirectory().then(data => {
        User.findById(req.params.id).populate('employee')
        .exec(function (err, result) {
            if (err) {
                console.log('error=>', err)
                return res.status(400).json({ message: err });
            }
            var employee = {
                position: '',
                marital: 'S',
                gender: false
            };
            if (result.employee){
                employee = result.employee;
            }
            
            res.render('employee/edit', {
                title: 'Employee information',
                model: result,
                employee: employee,
                dataOccupations: data.lstOccupations
            })
        });
    })
    */


});

const checkAddForm = [
    check('position','Enter position').isLength({min: 2, max: 200}),
    check('address','Enter address').isLength({min: 2, max: 200}),
    check('dob','Enter date of birth').not().isEmpty(),
    //check('occupations','Select at least one occupation').not().isEmpty(),
    check('phone_personal','Enter personal phone').isLength({min: 10, max: 20}),
    //check('phone_company','Enter company phone').isLength({min: 10, max: 20}),
  ];

router.post('/edit/:id', checkAddForm, async function(req, res, next) {

        const errors = validationResult(req);

        let user = await User.findById(req.params.id).populate('employee').exec()

        if (!errors.isEmpty()) {
            let dataOccupations = await Occupation.find({}).exec();

            let employee = req.body;
            //console.log('employee', employee);
            //if (req.body.dob) employee.dob = moment(req.body.dob, 'DD/MM/YYYY');
            if (user.employee) { 
                employee._id = user.employee._id
            }
            //console.log('employee', employee);
            //console.log(dataOccupations.length)
            return res.render('employee/edit', {
                title: 'Employee information',
                model: user,
                employee: employee,
                dataOccupations: dataOccupations,
                errors: errors.mapped()
            })
        }

        let updateEmployee = null;
        if (user.employee) {
            updateEmployee = user.employee;
        } else {
            updateEmployee = new Employee();
        }
        updateEmployee.occupations = req.body.occupations;
        updateEmployee.position = req.body.position;
        updateEmployee.dob = moment(req.body.dob, 'DD/MM/YYYY');
        updateEmployee.gender = req.body.gender;
        updateEmployee.marital = req.body.marital;
        updateEmployee.address = req.body.address;
        updateEmployee.phone_personal = req.body.phone_personal;
        updateEmployee.phone_company = req.body.phone_company;
        updateEmployee.notes = req.body.notes;
        //console.log('updateEmployee', updateEmployee)
        updateEmployee.save(function(err, result) {
            if (err) {
                console.log('error=>', err)
                return res.status(400).json({ message: err });
            }
            User.findByIdAndUpdate(req.params.id, {
                employee: result
            }, function(err1){
                if (err1) {
                    console.log('error=>', err1)
                    return res.status(400).json({ message: err1 });
                }
                res.redirect('/employees');
            })
    
        })
        
        
    });

router.get('/delete/:id/:emp_id', (req, res, next) => {
    Employee.findByIdAndDelete(req.params.emp_id, function (err) {
        if (err) {
            console.log('error=>', err)
            return res.status(400).json({ message: err });
        }
        User.findByIdAndUpdate(req.params.id, {
            employee: null
        }, function(err1){
            if (err1) {
                console.log('error=>', err1)
                return res.status(400).json({ message: err1 });
            }
            res.redirect('/employees');
        })
        
    });
});

module.exports = router;
