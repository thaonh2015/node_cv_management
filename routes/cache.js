const Occupation = require('../models/occupation');
const City = require('../models/city');
const Qualification = require('../models/qualification');
const Experience = require('../models/experience');
const Position = require('../models/position');
const Language = require('../models/language');
const JobType = require('../models/jobtype');
const User = require('../models/user');
var redisClient = require('redis').createClient;
const config = require('./../config')
const redis = redisClient(config.port_redis, config.url_redis);

module.exports.emptyAll = function() {
    redis.flushdb( function (err, succeeded) {
        console.log(succeeded); // will be true if successfull
    });
}

module.exports.emptyCache = function (__key) {
    redis.DEL(__key, function(err, o) {
        if (err) {
            console.log('err::', err)
        }
        console.log('result::',o)
    });
};

module.exports.findUsers = function (callback) {
    const __key = 'User';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('User loading...');
            User.find({}).sort('ordinal').exec(function(err, result){
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};

module.exports.findOccupations = function (callback) {
    const __key = 'Occupation';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('Occupation loading...');
            Occupation.find({}).sort('ordinal').exec(function(err, result){
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};

module.exports.findCities = function (callback) {
    const __key = 'City';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('City loading...');
            City.find({}).sort('ordinal').exec(function(err, result){
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};

module.exports.findQualifications = function (callback) {
    const __key = 'Qualification';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('Qualification loading...');
            Qualification.find({}).sort('ordinal').exec(function(err, result){
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};

module.exports.findExperiences = function (callback) {
    const __key = 'Experience';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('Experience loading...');
            Experience.find({}).sort('ordinal').exec(function(err, result){
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};

module.exports.findPositions = function (callback) {
    const __key = 'Position';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('Position loading...');
            Position.find({}).sort('ordinal').exec(function(err, result){
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};

module.exports.findLanguages = function (callback) {
    const __key = 'Language';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('Language loading...');
            Language.find({}).sort('ordinal').exec(function(err, result){
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};

module.exports.findJobTypes = function (callback) {
    const __key = 'JobType';
    redis.get(__key, function (err, reply) {
        if (err) callback(null);
        else if (reply) callback(JSON.parse(reply));
        else {
            console.log('JobType loading...');
            JobType.find({}).sort('ordinal').exec(function(err, result){
                //console.log(result)
                if (err) callback(null);
                redis.set(__key, JSON.stringify(result), function () {
                    callback(result);
                });
            })
        }
    });
};