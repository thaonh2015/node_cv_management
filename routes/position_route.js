const express = require('express');
const router = express.Router();
const Position = require('../models/position');
const { check, validationResult } = require('express-validator/check');

const cache = require('./cache');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('Position');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/positions');
});

router.get('/', (req, res, next) => {
Position.find({})
.sort({createdAt: -1})
.exec(function(err, result) {
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}

res.render('position/list', {
title: 'Danh sách Cấp bậc',
list: result
})
});
});

router.get('/add', (req, res, next) => {
res.render('position/add', {
title: 'Thông tin Cấp bậc',
model: {
name: '',
ordinal: 1
}
})
});

router.post('/add',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
return res.render('position/add', {
title: 'Thông tin Cấp bậc',
model: req.body,
errors: errors.mapped()
})
}

let model = new Position();
model.name = req.body.name;
model.ordinal = req.body.ordinal;
model.save(function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
if (req.body.save_cont != undefined) {
req.flash('info', 'Them thanh cong ' + result.name)
return res.redirect('/positions/add');
}
res.redirect('/positions');
});


});

router.get('/edit/:id', (req, res, next) => {
Position.findById(req.params.id, function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.render('position/edit', {
title: 'Thông tin Cấp bậc',
model: result
})
})

});

router.post('/edit/:id',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
let model = req.body;
model._id = req.params.id;
return res.render('position/edit', {
title: 'Thông tin Cấp bậc',
model: model,
errors: errors.mapped()
})
}

let model = {};
model.name = req.body.name;
model.ordinal = req.body.ordinal;

Position.findByIdAndUpdate(req.params.id, model, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/positions');
});
});

router.get('/delete/:id', (req, res, next) => {
Position.findByIdAndDelete(req.params.id, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/positions');
});
});

module.exports = router;
