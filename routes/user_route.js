const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Role = require('../models/role');
const { check, validationResult } = require('express-validator/check');
const bcrypt = require('bcryptjs');
const cache = require('./cache');
var mongoose = require('mongoose');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('User');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/users');
});

router.get('/', (req, res, next) => {
  var filter = {
    roles: {
      $not: {$elemMatch: { $eq: mongoose.Types.ObjectId('5bc02c75876e1b3efa434001') }}
    }
  };
  if (res.locals.isSuperAdmin){
    filter = {};
  }
  User.find(filter)
  .sort({createdAt: -1})
  .exec(function(err, result) {
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    
    res.render('user/list', {
      title: 'Account list',
      list: result
    })
  });
});

router.get('/add', (req, res, next) => {
  Role.find({role_name: { $ne: 'super_admin' }}, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.render('user/add', {
      title: 'Add new account',
      model: {
        name: '',
        email: '',
        password: '11111111',
        roles: []
      },
      roles: result
    })
  })
  
});

router.post('/add',
  [check('name','Name is required').not().isEmpty(),
  check('email','Email is required').not().isEmpty(),
  check('password','Name is required').not().isEmpty()], 
  async (req, res, next) => {
  
 
 
  let roles =  await Role.find({role_name: { $ne: 'super_admin' }}, function(err, result){
    if (err) {
      return [];
    }
    return result;
  })

  

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render('user/add', {
      title: 'Add new account',
      model: req.body,
      errors: errors.mapped(),
      roles: roles
    })
    
  } 

  let model = new User();
  model.name = req.body.name;
  model.email = req.body.email;
  model.password = req.body.password;
  model.roles = req.body.roles;
  model.active = true;
  model.save(function(err, result){
    
    if (err) {
      if (err.code === 11000) {
        return res.render('user/add', {
          title: 'Add new account',
          model: req.body,
          errors: {
            email: {
              msg: 'Email is registered already!'
            }
          },
          roles: roles
        })
      }
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    if (req.body.save_cont != undefined) {
      return res.redirect('/users/edit/' + result._id);
    }
    res.redirect('/users');
  });
 
  
});

router.get('/edit/:id', async (req, res, next) => {
  let roles =  await Role.find({role_name: { $ne: 'super_admin' }}, function(err, result){
    if (err) {
      return [];
    }
    return result;
  })
  User.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    result.password = '';
    //console.log('roles:', result.roles);
    //console.log('roles+:', result.roles.indexOf('5bc02c75876e1b3efa434003'));

    res.render('user/edit', {
      title: 'Edit account',
      model: result,
      roles: roles
    })
  })
  
});

router.post('/edit/:id', 
  [check('name','Name is required').not().isEmpty()], 
  async (req, res, next) => {

  let roles =  await Role.find({role_name: { $ne: 'super_admin' }}, function(err, result){
    if (err) {
      return [];
    }
    return result;
  })

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let model = req.body;
    model._id = req.params.id;
    model.name = req.body.name;
    model.email = req.body.email;
    model.password = req.body.password;
    model.roles = req.body.roles;
    return res.render('user/edit', {
      title: 'Edit account',
      model: model,
      roles: roles,
      errors: errors.mapped()
    })
  } 

  let model = {};
  model.name = req.body.name;
  model.roles = req.body.roles;
  if (req.body.password.trim() !== '') {
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password.trim(), salt);  
    model.password = hash;
  }

  User.findByIdAndUpdate(req.params.id, model, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    req.flash('info', 'Update account successfully');
    res.redirect('/users');
  });
});

router.get('/active/:id', (req, res, next) => {
  User.findByIdAndUpdate(req.params.id, {
    active: true
  }, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    req.flash('info', 'Active success')
    res.redirect('/users');
  });
  
});

router.get('/deactive/:id', (req, res, next) => {
  User.findByIdAndUpdate(req.params.id, {
    active: false
  }, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    req.flash('info', 'Deactive success')
    res.redirect('/users');
  });
  
});

router.get('/delete/:id', (req, res, next) => {
  User.findByIdAndDelete(req.params.id, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/users');
  });
});

module.exports = router;
