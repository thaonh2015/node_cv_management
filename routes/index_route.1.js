const express = require('express');
const router = express.Router();
const User = require('../models/user');
const Employee = require('../models/employee');
const { check, validationResult } = require('express-validator/check');
const auth = require('./auth');
const bcrypt = require('bcryptjs');

router.get('/', (req, res, next) => {
   //res.send('welcome!')
   res.redirect('/home');
});

router.get('/home', auth.checkSignIn, (req, res, next) => {
  console.log('index-route', res.locals)
  res.render('dashboard', {
    title: 'Tong quan',
  })
});

router.get('/register', auth.authenticated, (req, res, next) => {
  res.render('register', {
    title: 'Dang ky',
    model: {
      name: '',
      email: ''
    }
  })
});

router.post('/register',
[check('name','Ten bat buoc nhap').not().isEmpty(),
check('email','Email bat buoc nhap').isEmail(),
check('password1','Mat khau bat buoc nhap').not().isEmpty(),
check('password2', 'Mat khau khong trung khop')
  .not().isEmpty()
  .custom((value, { req }) => value === req.body.password1)
],
(req, res, next) => {
  
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render('register', {
      title: 'Dang ky',
      model: {
        name: req.body.name,
        email: req.body.email
      },
      errors: errors.mapped()
    })
  } 

  let model = new User();
  model.name = req.body.name;
  model.email = req.body.email;
  model.password = req.body.password1;
  model.roles = []

  model.save(function(err, result){
    if (err) {
      if (err.code === 11000) {
        return res.render('register', {
          title: 'Dang ky',
          model: {
            name: req.body.name,
            email: req.body.email
          },
          errors: {
            email: {
              msg: 'Email is registered already!'
            }
          }
        })
      }
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    req.flash('info', 'Ban da dang ky thanh cong! Xin vui long cho trong giay lat!');
    res.redirect('/register');
  });
 
});

router.get('/login', auth.authenticated, (req, res, next) => {
  //console.log(req.flash('error'))
  res.render('login', {
    title: 'Dang ky',
    model: {
      email: '', password: ''
    }
  })
});

router.post('/login',[
  check('email','Email bat buoc nhap').isEmail(),
  check('password','Mat khau bat buoc nhap').not().isEmpty(),
], async (req, res, next) => {
  
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render('login', {
      title: 'Dang nhap',
      model: {
        email: req.body.email
      },
      errors: errors.mapped()
    })
  }
  User.authenticate(req.body.email, req.body.password, async function (err, user) {
    if (err || !user) {
      return res.render('login', {
        title: 'Dang nhap',
        model: {
          email: req.body.email
        },
        errors: {
          unauthenticated: {
            msg: err
          }
        }
      })
    } 
    //console.log(user.roles)
    // load occupations
    /*
    if (user.employee) {
      var empl = await Employee.findById(user.employee._id).populate('occupations').exec()
      req.session.occupations = empl.occupations;
    } else {
      req.session.occupations = [];
    }
    */s
    //Employee.findById(user)
    
    req.session.loggedIn = user;

    var user_roles = [];
    user.roles.forEach(item => {
       user_roles.push(item.role_name);
    }) 
    req.session.user_roles = user_roles;

    return res.redirect('/home');
    
  });

  
});

router.get('/logout', (req, res, next) => {
  if (req.session) {
    // delete session object
    req.session.destroy(function (err) {
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      } else {
        //req.flash('info', 'You are logged out');
        res.redirect('/login');
      }
    });
  }
  
});

router.get('/profile', auth.checkSignIn, (req, res, next) => {
  res.render('profile', {
    title: 'Profile',
    model: {
      pass_current: '',
      pass_new: '',
      pass_confirm: ''
    }
  })
});

const checkChangePassForm = [
  check('pass_current','Enter current password').not().isEmpty(),
  check('pass_new','Enter new password (6-20 characters)').isLength({min: 6, max: 20}),
  check('pass_confirm', 'Confirm new password')
    .not().isEmpty()
    .custom((value, { req }) => value === req.body.pass_new)
];

router.post('/profile/change_password', auth.checkSignIn, checkChangePassForm, (req, res, next) => {
  
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render('profile', {
      title: 'Profile',
      model: {
        pass_current: '',
        pass_new: '',
        pass_confirm: ''
      },
      errors: errors.mapped()
    })
    
  } 
  console.log(res.locals.loggedIn)

  var matched = bcrypt.compareSync(req.body.pass_current, res.locals.loggedIn.password)
  if (!matched) {
    return res.render('profile', {
      title: 'Profile',
      model: {
        pass_current: '',
        pass_new: '',
        pass_confirm: ''
      },
      errors: {
        pass_current: {
          msg: 'Password current is wrong'
        }
      }
    })
  }

  
  var salt = bcrypt.genSaltSync(10);
  var hash = bcrypt.hashSync(req.body.pass_new.trim(), salt);  

  User.findByIdAndUpdate(res.locals.loggedIn._id, {
    password: hash
  }, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.locals.loggedIn.password = hash;
    req.flash('info', 'Change password successfully');
    res.redirect('/profile');
  });


});

router.get('/test-admin', (req, res, next) => {
  return res.status(200).json({ message: 'Test admin' });
});


module.exports = router;
