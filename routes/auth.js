function checkSignIn(req, res, next){
    if(req.session.loggedIn){
        next();     //If session exists, proceed to page
    } else {
        req.flash('info', 'Please sign in');
        res.redirect('/login');
    }
}

function authenticated(req, res, next){
    if(req.session.loggedIn){
        res.redirect('/home');
    } else {
        next();
    }
}
    

module.exports = {
    checkSignIn, authenticated
}