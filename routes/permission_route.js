const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const Permission = require('../models/permission');
const Role = require('../models/role');
var async = require('async');

router.get('/', (req, res, next) => {
    async.parallel({
      permissions: function(callback) {     
        Permission.find({}).exec(callback);
      },
      roles: function(callback) {     
        Role.find({}).exec(callback);
      }
    }, function(err, results) {
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.render('permission/list', {
        title: 'Permission',
        permissions: results.permissions,
        roles: results.roles
      })

    });
  
});

router.get('/edit_roles/:id', (req, res, next) => {
  Role.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.render('permission/role_edit', {
      title: 'Role edit',
      data: result
    })
  })
 
});

router.post('/edit_roles/:id', (req, res, next) => {
  console.log(req.body);
  let newPers = [];
  let pers = req.body.permissions_acl.split(',');
  pers.forEach(item => {
    let tmp = item.split(':');
    let val = '';
    if (req.body[tmp[0] + ':create']) {
      val += '1';
    } else {
      val += '0';
    }
    if (req.body[tmp[0] + ':read']) {
      val += '1';
    } else {
      val += '0';
    }
    if (req.body[tmp[0] + ':update']) {
      val += '1';
    } else {
      val += '0';
    }
    if (req.body[tmp[0] + ':delete']) {
      val += '1';
    } else {
      val += '0';
    }
    newPers.push(tmp[0] + ':' + val);
  })
  console.log(newPers);
  Role.findByIdAndUpdate(req.params.id, {
    permissions_acl: newPers
  }, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    req.flash('info', 'Update successsfully.');
    res.redirect('/permissions/edit_roles/' + req.params.id);
  });
  
 
});

router.get('/init-func', (req, res, next) => {
  
  var functions = [
    { permission_name: 'system_admin'}, //role, permission
    { permission_name: 'account'},
    { permission_name: 'city'}];
 
  Permission.collection.insert(functions, function (err, docs) {
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.status(201).json({ message: 'Created permissions successfully!'});
  });
});

router.get('/init-role', (req, res, next) => {
  
  
    var roles = [
      { role_name: 'super_admin', permissions_acl: ['system_admin:1111', 'account:1111', 'city:1111']}, 
      { role_name: 'admin', permissions_acl: ['city:1111']},
      { role_name: 'manager', permissions_acl: ['account:1111']},
      { role_name: 'employee', permissions_acl: []}];
 
  
  Role.collection.insert(roles, function (err, docs) {
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.status(201).json({ message: 'Created roles successfully!'});
  });
});


module.exports = router;
