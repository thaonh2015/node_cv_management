const express = require('express');
const router = express.Router();
const JobType = require('../models/jobtype');
const { check, validationResult } = require('express-validator/check');

const cache = require('./cache');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('JobType');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/jobtypes');
});

router.get('/', (req, res, next) => {
JobType.find({})
.sort({createdAt: -1})
.exec(function(err, result) {
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}

res.render('jobtype/list', {
title: 'Danh sách Loại việc',
list: result
})
});
});

router.get('/add', (req, res, next) => {
res.render('jobtype/add', {
title: 'Thông tin Loại việc',
model: {
name: '',
ordinal: 1
}
})
});

router.post('/add',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
return res.render('jobtype/add', {
title: 'Thông tin Loại việc',
model: req.body,
errors: errors.mapped()
})
}

let model = new JobType();
model.name = req.body.name;
model.ordinal = req.body.ordinal;
model.save(function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
if (req.body.save_cont != undefined) {
req.flash('info', 'Them thanh cong ' + result.name)
return res.redirect('/jobtypes/add');
}
res.redirect('/jobtypes');
});


});

router.get('/edit/:id', (req, res, next) => {
JobType.findById(req.params.id, function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.render('jobtype/edit', {
title: 'Thông tin Loại việc',
model: result
})
})

});

router.post('/edit/:id',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
let model = req.body;
model._id = req.params.id;
return res.render('jobtype/edit', {
title: 'Thông tin Loại việc',
model: model,
errors: errors.mapped()
})
}

let model = {};
model.name = req.body.name;
model.ordinal = req.body.ordinal;

JobType.findByIdAndUpdate(req.params.id, model, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/jobtypes');
});
});

router.get('/delete/:id', (req, res, next) => {
JobType.findByIdAndDelete(req.params.id, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/jobtypes');
});
});

module.exports = router;
