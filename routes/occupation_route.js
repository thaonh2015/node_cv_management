const express = require('express');
const router = express.Router();
const Occupation = require('../models/occupation');
const { check, validationResult } = require('express-validator/check');

const cache = require('./cache');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('Occupation');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/occupations');
});

router.get('/', (req, res, next) => {
Occupation.find({})
.sort({createdAt: -1})
.exec(function(err, result) {
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}

res.render('occupation/list', {
title: 'Danh sách Nghề nghiệp',
list: result
})
});
});

router.get('/add', (req, res, next) => {
res.render('occupation/add', {
title: 'Thông tin Nghề nghiệp',
model: {
name: '',
ordinal: 1
}
})
});

router.post('/add',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
return res.render('occupation/add', {
title: 'Thông tin Nghề nghiệp',
model: req.body,
errors: errors.mapped()
})
}

let model = new Occupation();
model.name = req.body.name;
model.ordinal = req.body.ordinal;
model.save(function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
if (req.body.save_cont != undefined) {
req.flash('info', 'Them thanh cong ' + result.name)
return res.redirect('/occupations/add');
}
res.redirect('/occupations');
});


});

router.get('/edit/:id', (req, res, next) => {
Occupation.findById(req.params.id, function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.render('occupation/edit', {
title: 'Thông tin Nghề nghiệp',
model: result
})
})

});

router.post('/edit/:id',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
let model = req.body;
model._id = req.params.id;
return res.render('occupation/edit', {
title: 'Thông tin Nghề nghiệp',
model: model,
errors: errors.mapped()
})
}

let model = {};
model.name = req.body.name;
model.ordinal = req.body.ordinal;

Occupation.findByIdAndUpdate(req.params.id, model, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/occupations');
});
});

router.get('/delete/:id', (req, res, next) => {
Occupation.findByIdAndDelete(req.params.id, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/occupations');
});
});

module.exports = router;
