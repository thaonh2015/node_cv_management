const express = require('express');
const router = express.Router();
var async = require('async');
const Candidate = require('../models/candidate');
const { check, validationResult } = require('express-validator/check');
//var redisClient = require('redis').createClient;
const cache = require('./cache');
const moment = require('moment');
var mongoose = require('mongoose');

//const URL_REDIS = process.env.URL_REDIS || 'localhost';
//var redis = redisClient(6379, URL_REDIS);

var multer = require('multer');

router.get('/', (req, res, next) => {
  loadDirectory()
  .then(direcs => {

    var filter = {
      //employee: res.locals.loggedIn.name
    };
    var filterText = {

    }
    if (res.locals.hasRoles(['employee'])) {
      filter.employee = res.locals.loggedIn.name
    } else {
      if (req.query.consultant && req.query.consultant!== '') {
        filter.employee= req.query.consultant; 
        filterText.consultant = req.query.consultant
      }
    }
    
    if (req.query.keyword) {
      filter.$or = [
        {name: new RegExp(req.query.keyword, "i")},
        {email: new RegExp(req.query.keyword, "i")},
        {phone: new RegExp(req.query.keyword, "i")},
        {work_title: new RegExp(req.query.keyword, "i")},
        {can_code: req.query.keyword},
      ]
      filterText.keyword = req.query.keyword
    }
    if (req.query.occupation && req.query.occupation!== '') {
      filter.occupation= req.query.occupation; 
      filterText.occupation = req.query.occupation
    }
    if (req.query.city && req.query.city!=='') {
      filter.cities = {
        $elemMatch: { $eq: req.query.city }
      }
      filterText.city = req.query.city
    }
    if (req.query.position && req.query.position!== '') {
      filter.position= req.query.position; 
      filterText.position = req.query.position
    }
    if (req.query.qualification && req.query.qualification!== '') {
      filter.qualification= req.query.qualification; 
      filterText.qualification = req.query.qualification
    }
    if (req.query.experience && req.query.experience!== '') {
      filter.experience= req.query.experience; 
      filterText.experience = req.query.experience
    }
    if (req.query.jobtype && req.query.jobtype!=='') {
      filter.job_types = {
        $elemMatch: { $eq: req.query.jobtype }
      }
      filterText.jobtype = req.query.jobtype
    }
    if (req.query.language && req.query.language!=='') {
      filter.languages = {
        $elemMatch: { $eq: req.query.language }
      }
      filterText.language = req.query.language
    }
    if (req.query.gender && req.query.gender!== '') {
      filter.gender= req.query.gender; 
      filterText.gender = req.query.gender
    }
    if (req.query.year_dob && req.query.year_dob!== '') {
      filter.dob = {
        "$gte": new Date(req.query.year_dob, 1, 1), 
        "$lt": new Date(req.query.year_dob, 12, 31)
      }
      filterText.year_dob = req.query.year_dob
    }
    if (req.query.salary_currency && req.query.salary_currency!== '') {
      filter.salary_currency= req.query.salary_currency; 
      filterText.salary_currency = req.query.salary_currency
      filter.salary_min = {
        "$lte": req.query.salary_expected
      }
      filter.salary_max = {
        "$gte": req.query.salary_expected
      }
      filterText.salary_expected = req.query.salary_expected
      
    }

    console.log('filter::', filter)

    var paging = {
      page_size: parseInt(req.query.page_size) || 5,
      page_number: parseInt(req.query.page_number) || 1,
      total_page: 0,
      number_records: 0
    }

    console.log(paging)

    async.parallel({
        count: function(callback) {     
          Candidate.count()
          .where(filter)
          .exec(callback);
        },
        list: function(callback) {
          Candidate.find().populate('occupation cities position qualification experience job_types languages')
          .where(filter)
          .skip((paging.page_size * paging.page_number) - paging.page_size)
          .limit(paging.page_size)
          .sort({createdAt: -1})  
          .exec(callback);
        }
      }, function(err, results) {
        if (err) { return next(err); }

        paging.total_page = Math.ceil(results.count / paging.page_size);
        paging.number_records = results.count;

        res.render('candidate/list', {
          title: 'Candidates',
          list: results.list,
          data: direcs,
          paging: paging,
          filterText: filterText
        }); 

      });
  }).catch(err => {
    return res.status(400).json({ message: err });
  }) 
  
});

function loadDirectory() {
  return new Promise((resolve, reject) => {
    async.parallel({
      lstOccupations: function(callback) {
        cache.findOccupations(function(data) {
          callback(null, data);
        })
      },
      lstCities: function(callback) {
        cache.findCities(function(data) {
          callback(null, data);
        })
      },
      lstQualifications: function(callback) {
        cache.findQualifications(function(data) {
          callback(null, data);
        })
      },
      lstExperiences: function(callback) {
        cache.findExperiences(function(data) {
          
          callback(null, data);
        })
      },
      lstPositions: function(callback) {
        cache.findPositions(function(data) {
          callback(null, data);
        })
      },
      lstLanguages: function(callback) {
        cache.findLanguages(function(data) {
          callback(null, data);
        })
      },
      lstJobTypes: function(callback) {
        cache.findJobTypes(function(data) {
          callback(null, data);
        })
      },
      lstUsers: function(callback) {
        cache.findUsers(function(data) {
          callback(null, data);
        })
      }
    }, function(err, results) {
      if (err) { 
        console.log(err);
        reject(err);
      }
      resolve(results);
    });
  });
  
}

function loadForm(page, title, res, model, errors){
  loadDirectory()
  .then(results => {
    return res.render('candidate/' + page, {
      title: title,
      model: model,
      errors: errors,
      lstOccupations: results.lstOccupations,
      lstCities: results.lstCities,
      lstQualifications: results.lstQualifications,
      lstExperiences: results.lstExperiences,
      lstPositions: results.lstPositions,
      lstLanguages: results.lstLanguages,
      lstJobTypes: results.lstJobTypes
    });
  }).catch(err => {
    return res.status(400).json({ message: err });
  }) 
  
}

// use redis cache pm2 start nodejs app
// https://www.sitepoint.com/caching-a-mongodb-database-with-redis/
router.get('/add', (req, res, next) => {
  var now = moment().format('DD/MM/YYYY')
  return loadForm('add', 'Add new candidate', res, {
    dob: now,
    submitedDate: now,
    gender: true,
    marital: 'S',
    available_time: now,
    can_code: ''
  });
  
});

const checkAddForm = [
    check('name','Enter name').isLength({min: 2, max: 200}),
    check('dob','Enter date of birth').not().isEmpty(),
    check('available_time','Enter available time to work').not().isEmpty(),
    check('short_education','Enter education in short').isLength({min: 1, max: 2000}),
    check('short_experience','Enter work experience in short').isLength({min: 1, max: 2000}),
    check('additional','Enter additional information').isLength({min: 1, max: 2000}),
    //check('can_code','Enter candidate code').isLength({min: 1, max: 30}),
    check('cities','Select at least one city to work').not().isEmpty(),
    check('work_title','Enter work title').isLength({min: 2, max: 200}),
    check('email','Enter email').isLength({min: 5, max: 100}),
    check('phone','Enter phone').isLength({min: 10, max: 20}),
    check('languages','Select at least one language').not().isEmpty(),
    check('job_types','Select at least one type of job to apply').not().isEmpty(),
    check('submitedDate','Enter submited date').not().isEmpty(),
  ];

router.post('/add', checkAddForm, async (req, res, next) => {
 
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return loadForm('add', 'Add new candidate', res, req.body, errors.mapped());
  } 

  let model = new Candidate();
  //cv info
  model.name = req.body.name;
  model.dob = moment(req.body.dob, 'DD/MM/YYYY');
  model.gender = req.body.gender;
  model.short_address = req.body.short_address;
  model.marital = req.body.marital;
  model.nationality = req.body.nationality;
  model.salary_current = req.body.salary_current | 0;
  model.current_currency = req.body.current_currency;
  model.salary_min = req.body.salary_min | 0;
  model.salary_max = req.body.salary_max | 0;
  model.salary_currency = req.body.salary_currency;
  model.available_time = moment(req.body.available_time, 'DD/MM/YYYY');
  model.short_education = req.body.short_education;
  model.short_experience = req.body.short_experience;
  model.additional = req.body.additional;
  //adv info
  //model.can_code = req.body.can_code;
  model.occupation = req.body.occupation;
  model.cities = req.body.cities;
  model.work_title = req.body.work_title;
  model.email = req.body.email;
  model.phone = req.body.phone;
  model.qualification = req.body.qualification;
  model.experience = req.body.experience;
  model.position = req.body.position;
  model.languages = req.body.languages;
  model.job_types = req.body.job_types;
  model.submitedDate = moment(req.body.submitedDate, 'DD/MM/YYYY');
  model.employee = res.locals.loggedIn.name;
  model.status = req.body.status;
  //process cv and photo

  var lastCan = await Candidate.findOne({}, {}, { sort: { '_id' : -1 } }, function(err, result) {
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })


  var no = lastCan === null? 0: new Number(lastCan.can_code.replace('THS',''));

  model.can_code = 'THS' + pad(no+1, 8);
  
  model.save(function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    if (req.body.save_cont != undefined) {
      req.flash('info', 'Add candidate success ' + result.name)
      return res.redirect('/candidates/add');
    }
    res.redirect('/candidates');
  });
 
  
});

function pad(num, size) {
  var s = num+"";
  while (s.length < size) s = "0" + s;
  return s;
}

router.get('/clone/:id', async (req, res, next) => {
  let result = await Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })
  result._id = mongoose.Types.ObjectId();
  result.isNew = true;
  let name = result.name.split(' - ');
  result.name = name[0] + ' - ' + moment().valueOf();
  //let can_code = result.can_code.split(' - ');
  //result.can_code = can_code[0] + ' - ' + moment().valueOf();

  var lastCan = await Candidate.findOne({}, {}, { sort: { '_id' : -1 } }, function(err, result) {
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })


  var no = lastCan === null? 0: new Number(lastCan.can_code.replace('THS',''));

  result.can_code = 'THS' + pad(no+1, 8);

  console.log(result);

  result.save(function(err, rs){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates');
  });
  
});

router.get('/edit/:id', (req, res, next) => {
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    let model = {};
    //cv info
    model._id = result._id;
    model.name = result.name;
    model.gender = result.gender;
    model.short_address = result.short_address;
    model.marital = result.marital;
    model.nationality = result.nationality;
    model.salary_current = result.salary_current | 0;
    model.current_currency = result.current_currency;
    model.salary_min = result.salary_min | 0;
    model.salary_max = result.salary_max | 0;
    model.salary_currency = result.salary_currency;
    model.short_education = result.short_education;
    model.short_experience = result.short_experience;
    model.additional = result.additional;
    //adv info
    //model.can_code = result.can_code;
    model.occupation = result.occupation;
    model.cities = result.cities;
    model.work_title = result.work_title;
    model.email = result.email;
    model.phone = result.phone;
    model.qualification = result.qualification;
    model.experience = result.experience;
    model.position = result.position;
    model.languages = result.languages;
    model.job_types = result.job_types;
    model.employee = result.employee;
    model.status = result.status;
    model.dob = moment(result.dob).format('DD/MM/YYYY')
    model.submitedDate = moment(result.submitedDate).format('DD/MM/YYYY')
    model.available_time = moment(result.available_time).format('DD/MM/YYYY')
  
    return loadForm('edit', 'Update candidate', res, model);
  })
  
});

router.get('/resume/:id', (req, res, next) => {
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.render('candidate/resume', {
      title: 'Candidate resume',
      model: result
    });
  })
  
});

router.get('/tmpl_hts/:id', (req, res, next) => {
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.render('candidate/tmpl_hts', {
      title: 'Candidate CV',
      model: result,
      position_applied: req.query.position_applied,
      language_cv: req.query.language_cv,
      moment: moment
    });
  })
  
});

router.get('/history/:id', (req, res, next) => {
  var now = moment().format('DD/MM/YYYY')
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.render('candidate/history', {
      title: 'Candidate history',
      model: result,
      expr: {
        from_date: now
      },
      show_form: false,
      moment: moment
    });
  })
  
});

router.get('/history_up/:candidate_id/:history_id', async (req, res, next) => {
 
  var cand = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (cand.work_experience) {
    var work_experience = cand.work_experience;
    for (var i=1; i< work_experience.length;i++) {
      if (work_experience[i]._id.equals(req.params.history_id)){
        var tmp = work_experience[i];
        work_experience[i] = work_experience[i-1];
        work_experience[i-1] = tmp;
        break;
      }
    }
    //console.log(work_experience)
    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
  
});

router.get('/history_down/:candidate_id/:history_id', async (req, res, next) => {
 
  var cand = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (cand.work_experience) {
    var work_experience = cand.work_experience;
    for (var i=0; i< work_experience.length-1;i++) {
      if (work_experience[i]._id.equals(req.params.history_id)){
        var tmp = work_experience[i];
        work_experience[i] = work_experience[i+1];
        work_experience[i+1] = tmp;
        break;
      }
    }
    console.log(work_experience)
    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {
    console.log('a')
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
  
});

router.get('/history_del/:candidate_id/:history_id', async (req, res, next) => {
 
  var cand = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (cand.work_experience) {
    var work_experience = cand.work_experience.filter(function(item) {
      return !item._id.equals(req.params.history_id)
    });
    
    console.log(work_experience)
    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {
    console.log('a')
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
  
});

router.get('/history_copy/:candidate_id/:history_id', async (req, res, next) => {
 
  var cand = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (cand.work_experience) {
    var found = cand.work_experience.filter(function(item) {
      return item._id.equals(req.params.history_id)
    });

    // found[0]._id = mongoose.Types.ObjectId();
    // found[0].isNew = true;
    // found[0].company += '-copy';
    var clone = {};
    clone.from_date = found[0].from_date;
    clone.to_date = found[0].to_date;
    clone.company = found[0].company + '-' + moment();
    clone.position_level = found[0].position_level;
    clone.main_duties = found[0].main_duties;
    cand.work_experience.push(clone);
    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: cand.work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {
    console.log('a')
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
  
});

router.get('/history_edit/:candidate_id/:history_id', async (req, res, next) => {
 
  var cand = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (cand.work_experience) {
    var found = cand.work_experience.filter(function(item) {
      return item._id.equals(req.params.history_id)
    });
    var expr = {};
    expr.from_date = moment(found[0].from_date).format('DD/MM/YYYY');
    if (found[0].to_date) {
      expr.to_date = moment(found[0].to_date).format('DD/MM/YYYY');
    }
    expr.company = found[0].company;
    expr.position_level = found[0].position_level;
    expr.main_duties = found[0].main_duties;
    expr._id = found[0]._id;

    return res.render('candidate/history', {
      title: 'Candidate history',
      model: cand,
      expr: expr,
      show_form: true,
      moment: moment
    });

  } else {
    //console.log('a')
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
  
});

const checkExprForm = [
  check('from_date','Enter from date').not().isEmpty(),
  check('company','Enter bussiness type or company work...').isLength({min: 1, max: 200}),
  check('position_level','Enter name position or level...').isLength({min: 1, max: 200}),
  check('main_duties','Enter main duties in short').isLength({min: 1, max: 2000}),
  
];

router.post('/history/:id', checkExprForm, async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    //console.log('a')
    Candidate.findById(req.params.id, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      return res.render('candidate/history', {
        title: 'Candidate history',
        model: result,
        expr: req.body,
        errors: errors.mapped(),
        show_form: true,
        moment: moment
      });
    })
  } else {
    console.log(req.body);
    let cand = await Candidate.findById(req.params.id, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      return result;
    })
    if (req.body._id) {
      for (var i=0; i< cand.work_experience.length;i++) {
        if (cand.work_experience[i]._id.equals(req.body._id)){
          cand.work_experience[i] = req.body;
          cand.work_experience[i].from_date = moment(req.body.from_date, 'DD/MM/YYYY');
          if (req.body.to_date) {
            cand.work_experience[i].to_date = moment(req.body.to_date, 'DD/MM/YYYY');
          } else {
            cand.work_experience[i].to_date = null;
          }
          break;
        }
      }
    } else {
      if (cand.work_experience) {
        cand.work_experience.push(req.body);
      } else {
        cand.work_experience = [req.body];
      }
    }
    
    Candidate.findByIdAndUpdate(req.params.id, {work_experience: cand.work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.id);
    })
    
  }

  
  
});


router.post('/edit/:id', checkAddForm, (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let model = req.body;
    model._id = req.params.id;
    return loadForm('edit', 'Update candidate', res, model, errors.mapped());
    
  } 

  let model = {};
  //cv info
  model.name = req.body.name;
  model.dob = moment(req.body.dob, 'DD/MM/YYYY');
  model.gender = req.body.gender;
  model.short_address = req.body.short_address;
  model.marital = req.body.marital;
  model.nationality = req.body.nationality;
  model.salary_current = req.body.salary_current | 0;
  model.current_currency = req.body.current_currency;
  model.salary_min = req.body.salary_min | 0;
  model.salary_max = req.body.salary_max | 0;
  model.salary_currency = req.body.salary_currency;
  model.available_time = moment(req.body.available_time, 'DD/MM/YYYY');
  model.short_education = req.body.short_education;
  model.short_experience = req.body.short_experience;
  model.additional = req.body.additional;
  //adv info
  model.can_code = req.body.can_code;
  model.occupation = req.body.occupation;
  model.cities = req.body.cities;
  model.work_title = req.body.work_title;
  model.email = req.body.email;
  model.phone = req.body.phone;
  model.qualification = req.body.qualification;
  model.experience = req.body.experience;
  model.position = req.body.position;
  model.languages = req.body.languages;
  model.job_types = req.body.job_types;
  model.submitedDate = moment(req.body.submitedDate, 'DD/MM/YYYY');
  model.employee = res.locals.loggedIn.name;
  model.status = req.body.status;

  Candidate.findByIdAndUpdate(req.params.id, model, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates');
  });
});

router.get('/delete/:id', (req, res, next) => {
  Candidate.findByIdAndDelete(req.params.id, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates');
  });
});

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
      cb(null, 'public/pdf')
  },
  filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
  }
});

var fileFilter = function (req, file, cb) {
  // accept image only
  if (!file.originalname.match(/\.(pdf)$/)) {
      return cb(new Error('Only pdf files are allowed!'), false);
  }
  cb(null, true);
};

var upload = multer({
  storage: storage,
  fileFilter: fileFilter,
  limits: {
      fileSize: 10 * 1000000
  }
});

router.post('/upload/:id', upload.single('resume_file'), function(req, res, next) {
  if (req.file == undefined) {
    return res.status(400).json({msg: 'No file is uploaded!'});
  }
  console.log('uploaded', req.file);
  Candidate.findByIdAndUpdate(req.params.id, {
    resume_url: req.file.filename
  }, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates/resume/' + req.params.id);
  });
  

});

module.exports = router;
