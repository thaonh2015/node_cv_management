const express = require('express');
const router = express.Router();
const City = require('../models/city');
const { check, validationResult } = require('express-validator/check');

const cache = require('./cache');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('City');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/cities');
});

router.get('/', (req, res, next) => {
  City.find({})
  .sort({createdAt: -1})
  .exec(function(err, result) {
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    
    res.render('city/list', {
      title: 'Danh sach noi lam viec',
      list: result
    })
  });
});

router.get('/add', (req, res, next) => {
  res.render('city/add', {
    title: 'Noi lam viec',
    model: {
      name: '',
      ordinal: 1
    }
  })
});

router.post('/add',
  [check('name','Ten bat buoc nhap').not().isEmpty(),
  check('ordinal','Thu tu hien thi bat buoc nhap').not().isEmpty()], 
  (req, res, next) => {
 
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render('city/add', {
      title: 'Noi lam viec',
      model: req.body,
      errors: errors.mapped()
    })
  } 

  let model = new City();
  model.name = req.body.name;
  model.ordinal = req.body.ordinal;
  model.save(function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    if (req.body.save_cont != undefined) {
      req.flash('info', 'Them thanh cong ' + result.name)
      return res.redirect('/cities/add');
    }
    res.redirect('/cities');
  });
 
  
});

router.get('/edit/:id', (req, res, next) => {
  City.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.render('city/edit', {
      title: 'Noi lam viec',
      model: result
    })
  })
  
});

router.post('/edit/:id', 
  [check('name','Ten bat buoc nhap').not().isEmpty(),
  check('ordinal','Thu tu hien thi bat buoc nhap').not().isEmpty()], 
  (req, res, next) => {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let model = req.body;
    model._id = req.params.id;
    return res.render('city/edit', {
      title: 'Noi lam viec',
      model: model,
      errors: errors.mapped()
    })
  } 

  let model = {};
  model.name = req.body.name;
  model.ordinal = req.body.ordinal;

  City.findByIdAndUpdate(req.params.id, model, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/cities');
  });
});

router.get('/delete/:id', (req, res, next) => {
  City.findByIdAndDelete(req.params.id, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/cities');
  });
});

module.exports = router;
