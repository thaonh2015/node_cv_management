const express = require('express');
const router = express.Router();
const Candidate = require('../models/candidate');
const Employee = require('../models/employee');
const { check, validationResult } = require('express-validator/check');
const cache = require('./cache');
const moment = require('moment');
var mongoose = require('mongoose');
var _ = require('lodash');
var multer = require('multer');
var asynclib = require('async');
const path = require('path');

const checkFormCandidate = [
  check('name','Enter name').isLength({min: 2, max: 200}),
  check('short_address','Enter short address').isLength({min: 2, max: 200}),
  check('dob','Enter date of birth').not().isEmpty(),
  check('available_time','Enter available time to work').not().isEmpty(),
  check('short_education','Enter education in short').isLength({min: 1, max: 2000}),
  check('short_experience','Enter work experience in short').isLength({min: 1, max: 2000}),
  check('additional','Enter additional information').isLength({min: 1, max: 2000}),
  check('cities','Select at least one city to work').not().isEmpty(),
  check('occupations','Select at least one occupation').not().isEmpty(),
  check('work_title','Enter work title').isLength({min: 2, max: 200}),
  check('email','Enter email').isLength({min: 5, max: 100}),
  check('phone','Enter phone').isLength({min: 10, max: 20}),
  check('languages','Select at least one language').not().isEmpty(),
  check('job_types','Select at least one type of job to apply').not().isEmpty(),
  check('submitedDate','Enter submited date').not().isEmpty(),
];

const checkFormNote = [
  check('modifiedAt','Enter date').not().isEmpty(),
  check('content','Enter content').isLength({min: 1, max: 2000}),
  
];

const checkFormHistory = [
  check('from_date','Enter from date').not().isEmpty(),
  check('company','Enter bussiness type or company work...').isLength({min: 1, max: 200}),
  check('position_level','Enter name position or level...').isLength({min: 1, max: 200}),
  check('main_duties','Enter main duties in short').isLength({min: 1, max: 2000}),
];

// var storage = multer.diskStorage({
//   destination: function (req, file, cb) {
//       cb(null, 'public/pdf')
//   },
//   filename: function (req, file, cb) {
//       cb(null, file.fieldname + '-' + Date.now() + '-' + file.originalname);
//   }
// });

// var fileFilter = function (req, file, cb) {
//   // accept image only
//   if (!file.originalname.match(/\.(pdf)$/)) {
//       return cb(new Error('Only pdf files are allowed!'), false);
//   }
//   cb(null, true);
// };

// var upload = multer({
//   storage: storage,
//   fileFilter: fileFilter,
//   limits: {
//       fileSize: 10 * 1000000
//   }
// });

router.get('/', showListCandidate);
router.get('/add', showAddForm);
router.post('/add', checkFormCandidate, createCandidate);
router.get('/edit/:id', showEditForm);
router.post('/edit/:id', checkFormCandidate, editCandidate);
router.get('/delete/:id', deleteCandidate);
router.get('/resume/:id', viewResume);
router.get('/notes/:id', showListNote);
router.post('/notes/:id', checkFormNote, createNote);
router.get('/notes_edit/:candidate_id/:note_id', showNoteEdit);
router.get('/notes_delete/:candidate_id/:note_id', deleteNote);
router.get('/history/:id', showListHistory);
router.post('/history/:id', checkFormHistory, createHistory);
router.get('/history_edit/:candidate_id/:history_id', showHistoryEdit);
router.get('/history_del/:candidate_id/:history_id', deleteHistory);
router.get('/history_copy/:candidate_id/:history_id', copyHistory);
router.get('/history_up/:candidate_id/:history_id', upHistory);
router.get('/history_down/:candidate_id/:history_id', downHistory);
router.get('/tmpl_hts/:id', exportHTS);


module.exports = router;

async function showListCandidate(req, res, next) {
  
  var filter = {
    //employee: res.locals.loggedIn.name
  };
  var filterText = {

  }
  

  if (req.query.keyword) {
    filter.$or = [
      {name: new RegExp(req.query.keyword, "i")},
      {email: new RegExp(req.query.keyword, "i")},
      {phone: new RegExp(req.query.keyword, "i")},
      {work_title: new RegExp(req.query.keyword, "i")},
      {can_code: req.query.keyword},
    ]
    filterText.keyword = req.query.keyword
  }
/*
  if (req.query.occupation && req.query.occupation!== '') {
    filter.occupations = {
      $elemMatch: { $eq: req.query.occupation }
    }
    filterText.occupation = req.query.occupation
  } 
  */

  if (req.query.city && req.query.city!=='') {
    filter.cities = {
      $elemMatch: { $eq: req.query.city }
    }
    filterText.city = req.query.city
  }
  if (req.query.position && req.query.position!== '') {
    filter.position= req.query.position; 
    filterText.position = req.query.position
  }
  if (req.query.qualification && req.query.qualification!== '') {
    filter.qualification= req.query.qualification; 
    filterText.qualification = req.query.qualification
  }
  if (req.query.experience && req.query.experience!== '') {
    filter.experience= req.query.experience; 
    filterText.experience = req.query.experience
  }
  if (req.query.jobtype && req.query.jobtype!=='') {
    filter.job_types = {
      $elemMatch: { $eq: req.query.jobtype }
    }
    filterText.jobtype = req.query.jobtype
  }
  if (req.query.language && req.query.language!=='') {
    filter.languages = {
      $elemMatch: { $eq: req.query.language }
    }
    filterText.language = req.query.language
  }
  if (req.query.gender && req.query.gender!== '') {
    filter.gender= req.query.gender; 
    filterText.gender = req.query.gender
  }
  if (req.query.year_dob && req.query.year_dob!== '') {
    filter.dob = {
      "$gte": new Date(req.query.year_dob, 1, 1), 
      "$lt": new Date(req.query.year_dob, 12, 31)
    }
    filterText.year_dob = req.query.year_dob
  }
  if (req.query.salary_currency && req.query.salary_currency!== '') {
    filter.salary_currency= req.query.salary_currency; 
    filterText.salary_currency = req.query.salary_currency
    filter.salary_min = {
      "$lte": req.query.salary_expected
    }
    filter.salary_max = {
      "$gte": req.query.salary_expected
    }
    filterText.salary_expected = req.query.salary_expected
    
  }

  var paging = {
    page_size: parseInt(req.query.page_size) || 5,
    page_number: parseInt(req.query.page_number) || 1,
    total_page: 0,
    number_records: 0
  }

  var directs = await loadDirectory(data=>{return data;});

  if (res.locals.isManagerCommon) {
    // no change list occupation
    console.log('all occupations for admin')
  } else if ('employee' in res.locals.loggedIn) {
    console.log('restrict occupations for employee')
    //console.log(res.locals.loggedIn);
    // only employee role
    var empl = await Employee.findById(res.locals.loggedIn.employee).populate('occupations').exec()
    //console.log(empl);
    directs.lstOccupations = empl.occupations;
  } else {
    console.log('not yet assign occupation for consultant');
    directs.lstOccupations = [];
  }

  if (req.query.occupation && req.query.occupation!== '') {
    filter.occupations = {
      $elemMatch: { $eq: req.query.occupation }
    }
    filterText.occupation = req.query.occupation
  } else {
    if (!res.locals.isManagerCommon) {
      filter.occupations = {
        $elemMatch: { $in: directs.lstOccupations }
      }
    }
  }

  let candidateSum = await Candidate.find({}).populate('occupations')
  .select('name occupations')
  .where(filter).exec();
  //console.log('candidateSum', candidateSum);
  let mapSum = {};
  candidateSum.forEach(can => {
    can.occupations.forEach(occ => {
      //console.log('occ', occ)
      if (occ._id in mapSum) {
        //mapSum[occ].count = mapSum[occ].count++;
        //console.log('yes inscr')
        mapSum[occ._id]++;
      } else {
        //mapSum[occ].count = 0;
        //console.log('start')
        mapSum[occ._id] = 1;
      }
    })
  })
  //console.log(mapSum);

  var groupOccupation = _.groupBy(directs.lstOccupations, 'group');
  var groupOccupationWithNo = _.mapValues(groupOccupation, function(item) {
    let listNo = [];
    item.forEach(occ => {
      if (occ._id in mapSum) {
        listNo.push({_id: occ._id, name: occ.name, no: mapSum[occ._id]})
      } else {
        listNo.push({_id: occ._id, name: occ.name, no: 0})
      }
      
    })
    return listNo;
  });

  //console.log('filter occupation:: ', directs.lstOccupations)

  if (req.query.consultant && req.query.consultant!== '') {
    filter.notes = {
      $elemMatch: { consultant: req.query.consultant }
    }
    filterText.consultant = req.query.consultant
  }

  console.log('filter', filter)

  asynclib.parallel({
    count: function(callback) {     
      Candidate.count()
      .where(filter)
      .exec(callback);
    },
    list: function(callback) {
      Candidate.find().populate('occupations cities position qualification experience job_types languages notes')
      .where(filter)
      .skip((paging.page_size * paging.page_number) - paging.page_size)
      .limit(paging.page_size)
      .sort({createdAt: -1})  
      .exec(callback);
    }
  }, function(err, results) {
    if (err) { return next(err); }

    paging.total_page = Math.ceil(results.count / paging.page_size);
    paging.number_records = results.count;

    res.render('candidate/list', {
      title: 'Candidates',
      list: results.list,
      data: directs,
      paging: paging,
      filterText: filterText,
      occGroup: groupOccupationWithNo
    }); 

  });

}

function loadDirectory() {
  return new Promise((resolve, reject) => {
    asynclib.parallel({
      lstOccupations: function(callback) {
        cache.findOccupations(function(data) {
          callback(null, data);
        })
      },
      lstCities: function(callback) {
        cache.findCities(function(data) {
          callback(null, data);
        })
      },
      lstQualifications: function(callback) {
        cache.findQualifications(function(data) {
          callback(null, data);
        })
      },
      lstExperiences: function(callback) {
        cache.findExperiences(function(data) {
          
          callback(null, data);
        })
      },
      lstPositions: function(callback) {
        cache.findPositions(function(data) {
          callback(null, data);
        })
      },
      lstLanguages: function(callback) {
        cache.findLanguages(function(data) {
          callback(null, data);
        })
      },
      lstJobTypes: function(callback) {
        cache.findJobTypes(function(data) {
          callback(null, data);
        })
      },
      lstUsers: function(callback) {
        cache.findUsers(function(data) {
          callback(null, data);
        })
      }
    }, function(err, results) {
      if (err) { 
        console.log(err);
        reject(err);
      }
      resolve(results);
    });
  });
  
}

async function showAddForm(req, res, next) {
  var now = moment().format('DD/MM/YYYY')
  var model = {
    dob: now,
    submitedDate: now,
    gender: true,
    marital: 'S',
    available_time: now,
    can_code: '',
    short_education: 'Summary Education:',
    short_experience: 'Summary Experience:',
    additional: 'Additional Information:',
    short_address: 'TPHCM'
  };
  return buildCandidateForm('add', 'Add new candidate', res, model)
}

async function buildCandidateForm(page, title, res, model, errors) {
  var directs = await loadDirectory(data=>{return data;});
  var dataOccupations = [];
  if (res.locals.isManagerCommon) {
    dataOccupations = directs.lstOccupations;
  } else if ('employee' in res.locals.loggedIn) {
    console.log('restrict occupations for employee')
    var empl = await Employee.findById(res.locals.loggedIn.employee).populate('occupations').exec()
    dataOccupations = empl.occupations;
  } 
  return res.render('candidate/' + page, {
    title: title,
    model: model,
    errors: errors,
    lstOccupations: dataOccupations,
    lstCities: directs.lstCities,
    lstQualifications: directs.lstQualifications,
    lstExperiences: directs.lstExperiences,
    lstPositions: directs.lstPositions,
    lstLanguages: directs.lstLanguages,
    lstJobTypes: directs.lstJobTypes
  });
  
}

async function createCandidate(req, res, next) {

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return buildCandidateForm('add', 'Add new candidate', res, req.body, errors.mapped());
  } 

  if (Object.keys(req.files).length == 0) {
    //return res.status(400).json({msg: 'No file is uploaded!'});
    return buildCandidateForm('add', 'Add new candidate', res, req.body, {
      resume_file: {
        msg: 'File CV is required!'
      }
    });
  }

  let sampleFile = req.files.resume_file;
  let fileName = 'resume_file-' + Date.now() + '-' + req.files.resume_file.name;

  // Use the mv() method to place the file somewhere on your server
  
  await sampleFile.mv(path.resolve(`./public/pdf/${fileName}`), function(err) {
    if (err)
      return res.status(500).send(err);
    console.log('Upload finish');
  });

  let model = new Candidate(req.body);
  model.resume_url = fileName;
  model.dob = moment(req.body.dob, 'DD/MM/YYYY');
  model.available_time = moment(req.body.available_time, 'DD/MM/YYYY');
  model.submitedDate = moment(req.body.submitedDate, 'DD/MM/YYYY');
  model.salary_current = req.body.salary_current | 0;
  model.salary_min = req.body.salary_min | 0;
  model.salary_max = req.body.salary_max | 0;
  model.can_code = await generateCode();
  console.log(model.can_code)
  
  model.save(function(err, result){
    if (err) {
      console.log('error=>', err)
      if (err.code === 11000) {
        return buildCandidateForm('add', 'Add new candidate', res, req.body, {
          email: {
            msg: 'Email is registered already!'
          }
        });
      }
      return res.status(400).json({ message: err });
    }
    if (req.body.save_cont != undefined) {
      req.flash('info', 'Add candidate success ' + result.name)
      return res.redirect('/candidates/add');
    }
    res.redirect('/candidates');
  });
}

async function generateCode() {
  var lastCan = await Candidate.findOne({}, {}, { sort: { '_id' : -1 } }, function(err, result) {
    if (err) {
      console.log('error=>', err)
     return '';
    }
    return result;
  })

  var no = lastCan === null? 0: new Number(lastCan.can_code.substr(3));
  var pad = (no + 1) + "";
  while (pad.length < 8) {
    pad = "0" + pad;
  }
  return 'HTS' + pad;
}

async function showEditForm(req, res, next) {
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    let model = result.toJSON();
    model.dob = moment(result.dob).format('DD/MM/YYYY')
    model.submitedDate = moment(result.submitedDate).format('DD/MM/YYYY')
    model.available_time = moment(result.available_time).format('DD/MM/YYYY')
  
    return buildCandidateForm('edit', 'Update candidate', res, model);
  })
  
}

async function editCandidate(req, res, next) {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    let model = req.body;
    model._id = req.params.id;
    return buildCandidateForm('edit', 'Update candidate', res, model, errors.mapped());
    
  } 

  let model = req.body;
  //cv info
  model.dob = moment(req.body.dob, 'DD/MM/YYYY');
  model.available_time = moment(req.body.available_time, 'DD/MM/YYYY');
  model.submitedDate = moment(req.body.submitedDate, 'DD/MM/YYYY');
 

  Candidate.findByIdAndUpdate(req.params.id, model, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates');
  });
}

async function deleteCandidate(req, res, next){
  Candidate.findByIdAndDelete(req.params.id, function(err){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates');
  });
}

async function viewResume(req, res, next) {
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.render('candidate/resume', {
      title: 'Candidate resume',
      model: result
    });
  })
}

async function showListNote(req, res, next) {
  var now = moment().format('DD/MM/YYYY')
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.render('candidate/notes', {
      title: 'Candidate note',
      model: result,
      note: {
        modifiedAt: now,
        consultant: res.locals.loggedIn.name
      },
      show_form: false,
      moment: moment
    });
  })
}

async function createNote(req, res, next) {

  let foundCandidate = await Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
    }
    return result;
  });

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render('candidate/notes', {
      title: 'Candidate note',
      model: foundCandidate,
      note: req.body,
      errors: errors.mapped(),
      show_form: true,
      moment: moment
    });
  } 
  
  if (req.body._id) {
    for (var i=0; i< foundCandidate.notes.length;i++) {
      if (foundCandidate.notes[i]._id.equals(req.body._id)){
        foundCandidate.notes[i] = req.body;
        foundCandidate.notes[i].modifiedAt = moment(req.body.modifiedAt, 'DD/MM/YYYY');
        break;
      }
    }
  } else {
    if (foundCandidate.notes) {
      foundCandidate.notes.push(req.body);
    } else {
      foundCandidate.notes = [req.body];
    }
  }
  
  Candidate.findByIdAndUpdate(req.params.id, {notes: foundCandidate.notes}, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates/notes/' + req.params.id);
  })
}

async function showNoteEdit(req, res, next) {
  var foundCandidate = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (foundCandidate.notes) {
    var found = foundCandidate.notes.filter(function(item) {
      return item._id.equals(req.params.note_id)
    });
    var note = {};
    note.modifiedAt = moment(found[0].modifiedAt).format('DD/MM/YYYY');
    note.consultant = found[0].consultant;
    note.content = found[0].content;
    note._id = found[0]._id;

    return res.render('candidate/notes', {
      title: 'Candidate notes',
      model: foundCandidate,
      note: note,
      show_form: true,
      moment: moment
    });

  } 

  res.redirect('/candidates/notes/' + req.params.candidate_id);
  
}

async function deleteNote(req, res, next) {
  var foundCandidate = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (foundCandidate.notes) {
    var notes = foundCandidate.notes.filter(function(item) {
      return !item._id.equals(req.params.note_id)
    });
    
    Candidate.findByIdAndUpdate(req.params.candidate_id, {notes: notes}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/notes/' + req.params.candidate_id);
    })
  } else {
    
    res.redirect('/candidates/notes/' + req.params.candidate_id);
  }
}

async function showListHistory(req, res, next){
  var now = moment().format('DD/MM/YYYY')
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.render('candidate/history', {
      title: 'Candidate history',
      model: result,
      expr: {
        from_date: now
      },
      show_form: false,
      moment: moment
    });
  })
}

async function createHistory(req, res, next){

  let foundCandidate = await Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.render('candidate/history', {
      title: 'Candidate history',
      model: foundCandidate,
      expr: req.body,
      errors: errors.mapped(),
      show_form: true,
      moment: moment
    });

  } 

  if (req.body._id) {
    for (var i=0; i< foundCandidate.work_experience.length;i++) {
      if (foundCandidate.work_experience[i]._id.equals(req.body._id)){
        foundCandidate.work_experience[i] = req.body;
        foundCandidate.work_experience[i].from_date = moment(req.body.from_date, 'DD/MM/YYYY');
        if (req.body.to_date) {
          foundCandidate.work_experience[i].to_date = moment(req.body.to_date, 'DD/MM/YYYY');
        } else {
          foundCandidate.work_experience[i].to_date = null;
        }
        break;
      }
    }
  } else {
    if (foundCandidate.work_experience) {
      foundCandidate.work_experience.push(req.body);
    } else {
      foundCandidate.work_experience = [req.body];
    }
  }
  
  Candidate.findByIdAndUpdate(req.params.id, {work_experience: foundCandidate.work_experience}, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    res.redirect('/candidates/history/' + req.params.id);
  })
}

async function showHistoryEdit(req, res, next){
  var foundCandidate = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (foundCandidate.work_experience) {
    var found = foundCandidate.work_experience.filter(function(item) {
      return item._id.equals(req.params.history_id)
    });
    var expr = {};
    expr.from_date = moment(found[0].from_date).format('DD/MM/YYYY');
    if (found[0].to_date) {
      expr.to_date = moment(found[0].to_date).format('DD/MM/YYYY');
    }
    expr.company = found[0].company;
    expr.position_level = found[0].position_level;
    expr.main_duties = found[0].main_duties;
    expr._id = found[0]._id;

    return res.render('candidate/history', {
      title: 'Candidate history',
      model: foundCandidate,
      expr: expr,
      show_form: true,
      moment: moment
    });

  } else {
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
}

async function deleteHistory(req, res, next){
  var foundCandidate = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (foundCandidate.work_experience) {
    var work_experience = foundCandidate.work_experience.filter(function(item) {
      return !item._id.equals(req.params.history_id)
    });
    
    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
}

async function copyHistory(req, res, next){

  var foundCandidate = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (foundCandidate.work_experience) {
    var found = foundCandidate.work_experience.filter(function(item) {
      return item._id.equals(req.params.history_id)
    });

    var clone = {};
    clone.from_date = found[0].from_date;
    clone.to_date = found[0].to_date;
    clone.company = found[0].company + '-' + moment();
    clone.position_level = found[0].position_level;
    clone.main_duties = found[0].main_duties;
    
    foundCandidate.work_experience.push(clone);

    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: foundCandidate.work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
}

async function upHistory(req, res, next){
  var foundCandidate = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (foundCandidate.work_experience) {
    var work_experience = foundCandidate.work_experience;
    for (var i=1; i< work_experience.length;i++) {
      if (work_experience[i]._id.equals(req.params.history_id)){
        var tmp = work_experience[i];
        work_experience[i] = work_experience[i-1];
        work_experience[i-1] = tmp;
        break;
      }
    }
    //console.log(work_experience)
    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {
    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
}

async function downHistory(req, res, next){

  var foundCandidate = await Candidate.findById(req.params.candidate_id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return result;
  })

  if (foundCandidate.work_experience) {
    var work_experience = foundCandidate.work_experience;
    for (var i=0; i< work_experience.length-1;i++) {
      if (work_experience[i]._id.equals(req.params.history_id)){
        var tmp = work_experience[i];
        work_experience[i] = work_experience[i+1];
        work_experience[i+1] = tmp;
        break;
      }
    }

    Candidate.findByIdAndUpdate(req.params.candidate_id, {work_experience: work_experience}, function(err, result){
      if (err) {
        console.log('error=>', err)
        return res.status(400).json({ message: err });
      }
      res.redirect('/candidates/history/' + req.params.candidate_id);
    })
  } else {

    res.redirect('/candidates/history/' + req.params.candidate_id);
  }
}

async function exportHTS(req, res, next) {
  Candidate.findById(req.params.id, function(err, result){
    if (err) {
      console.log('error=>', err)
      return res.status(400).json({ message: err });
    }
    return res.render('candidate/tmpl_hts', {
      title: 'Candidate CV',
      model: result,
      position_applied: req.query.position_applied,
      language_cv: req.query.language_cv,
      moment: moment
    });
  });
}
