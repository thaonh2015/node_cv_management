const express = require('express');
const router = express.Router();
const Language = require('../models/language');
const { check, validationResult } = require('express-validator/check');

const cache = require('./cache');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('Language');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/languages');
});

router.get('/', (req, res, next) => {
Language.find({})
.sort({createdAt: -1})
.exec(function(err, result) {
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}

res.render('language/list', {
title: 'Danh sách Ngoại ngữ',
list: result
})
});
});

router.get('/add', (req, res, next) => {
res.render('language/add', {
title: 'Thông tin Ngoại ngữ',
model: {
name: '',
ordinal: 1
}
})
});

router.post('/add',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
return res.render('language/add', {
title: 'Thông tin Ngoại ngữ',
model: req.body,
errors: errors.mapped()
})
}

let model = new Language();
model.name = req.body.name;
model.ordinal = req.body.ordinal;
model.save(function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
if (req.body.save_cont != undefined) {
req.flash('info', 'Them thanh cong ' + result.name)
return res.redirect('/languages/add');
}
res.redirect('/languages');
});


});

router.get('/edit/:id', (req, res, next) => {
Language.findById(req.params.id, function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.render('language/edit', {
title: 'Thông tin Ngoại ngữ',
model: result
})
})

});

router.post('/edit/:id',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
let model = req.body;
model._id = req.params.id;
return res.render('language/edit', {
title: 'Thông tin Ngoại ngữ',
model: model,
errors: errors.mapped()
})
}

let model = {};
model.name = req.body.name;
model.ordinal = req.body.ordinal;

Language.findByIdAndUpdate(req.params.id, model, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/languages');
});
});

router.get('/delete/:id', (req, res, next) => {
Language.findByIdAndDelete(req.params.id, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/languages');
});
});

module.exports = router;
