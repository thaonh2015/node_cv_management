const express = require('express');
const router = express.Router();
const Experience = require('../models/experience');
const { check, validationResult } = require('express-validator/check');

const cache = require('./cache');

router.get('/emptyCache', (req, res, next) => {
  cache.emptyCache('Experience');
  req.flash('info', 'Empty cache successfully');
  res.redirect('/experiences');
});

router.get('/', (req, res, next) => {
Experience.find({})
.sort({createdAt: -1})
.exec(function(err, result) {
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}

res.render('experience/list', {
title: 'Danh sách Kinh nghiệm',
list: result
})
});
});

router.get('/add', (req, res, next) => {
res.render('experience/add', {
title: 'Thông tin Kinh nghiệm',
model: {
name: '',
ordinal: 1
}
})
});

router.post('/add',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
return res.render('experience/add', {
title: 'Thông tin Kinh nghiệm',
model: req.body,
errors: errors.mapped()
})
}

let model = new Experience();
model.name = req.body.name;
model.ordinal = req.body.ordinal;
model.save(function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
if (req.body.save_cont != undefined) {
req.flash('info', 'Them thanh cong ' + result.name)
return res.redirect('/experiences/add');
}
res.redirect('/experiences');
});


});

router.get('/edit/:id', (req, res, next) => {
Experience.findById(req.params.id, function(err, result){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.render('experience/edit', {
title: 'Thông tin Kinh nghiệm',
model: result
})
})

});

router.post('/edit/:id',
[check('name','Name is required').not().isEmpty(),
check('ordinal','Ordinal is required').not().isEmpty()],
(req, res, next) => {

const errors = validationResult(req);
if (!errors.isEmpty()) {
let model = req.body;
model._id = req.params.id;
return res.render('experience/edit', {
title: 'Thông tin Kinh nghiệm',
model: model,
errors: errors.mapped()
})
}

let model = {};
model.name = req.body.name;
model.ordinal = req.body.ordinal;

Experience.findByIdAndUpdate(req.params.id, model, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/experiences');
});
});

router.get('/delete/:id', (req, res, next) => {
Experience.findByIdAndDelete(req.params.id, function(err){
if (err) {
console.log('error=>', err)
return res.status(400).json({ message: err });
}
res.redirect('/experiences');
});
});

module.exports = router;
